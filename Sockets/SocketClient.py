import json
import random
import re
import socket
import threading
import string
from AES import *
from RSA import *

from Protocol import Protocol

from Button import *


class Lobby:
    def __init__(self, users=None, game=None, scores=None, state=None, id=None, placeholder=False) -> None:
        """
        Initializes a Lobby object.

        :param users: List of users in the lobby.
        :param game: Game being played in the lobby.
        :param scores: Scores of users in the lobby.
        :param state: Current state of the lobby.
        :param id: Identifier of the lobby.
        :param placeholder: Flag indicating if the lobby is a placeholder.
        """
        self.placeholder = placeholder
        if placeholder:
            return

        self.users = users
        self.game = game
        self.scores = scores
        self.state = state
        self.id = id

        self.cur_pos: list = []
        self.height = 0
        self.width = 0

    def update_values(self, list_data: list) -> None:
        """
        Updates the values of the Lobby object based on the provided list data.

        :param list_data: List containing lobby data to update.
        """
        self.users = list_data[1]
        self.game = list_data[2]
        self.scores = list_data[3]
        self.state = list_data[4]
        self.id = list_data[0]

    def copy_lobby(self, other_lobby) -> None:
        """
        Sets the values of the current lobby to those of another lobby.

        :param other_lobby: Another Lobby object to copy values from.
        """
        self.users = other_lobby.users
        self.game = other_lobby.game
        self.scores = other_lobby.scores
        self.state = other_lobby.state
        self.id = other_lobby.id

        self.cur_pos: list = other_lobby.cur_pos
        self.height = other_lobby.height
        self.width = other_lobby.width

    def is_clicked(self, mouse_x, mouse_y):
        """
        Checks if the lobby is clicked by the mouse.

        :param mouse_x: X-coordinate of the mouse.
        :param mouse_y: Y-coordinate of the mouse.
        :return: Lobby object if clicked, None otherwise.
        """
        if not (600 > mouse_x > 100 and 700 > mouse_y > 300):
            return None
        try:
            if (self.cur_pos[0] <= mouse_x <= self.cur_pos[0] + self.width) and (
                    self.cur_pos[1] <= mouse_y <= self.cur_pos[1] + self.height):
                return self
        except IndexError:
            return None

    def draw_preview(self, window, color, position: tuple[int, int], width, height, text_pos: tuple[int, int],
                     size: int) -> None:
        """
        Draws a preview of the lobby.

        :param window: Pygame surface to draw on.
        :param color: Color of the lobby preview.
        :param position: Position of the lobby preview.
        :param width: Width of the lobby preview.
        :param height: Height of the lobby preview.
        :param text_pos: Position of the lobby text.
        :param size: Font size of the lobby text.
        """
        pygame.draw.rect(window, (255, 255, 255), (position, (width, height)))
        pygame.draw.rect(window, color, (position, (width, height)), width=5)

        # pygame.display.set_caption('Show Text')
        font = pygame.font.Font('freesansbold.ttf', size)
        text = font.render(f'users: {self.users}', True, color)
        text_rect = text.get_rect()
        text_rect.topleft = text_pos
        window.blit(text, text_rect)

        text2 = font.render(f'game: {self.game}', True, color)
        text_rect2 = text.get_rect()
        text_rect2.topleft = (text_pos[0], text_pos[1] + size + 5)
        window.blit(text2, text_rect2)

        self.cur_pos = list(position)
        self.height = height
        self.width = width

    def draw_lobby(self, window) -> None:
        """
        Draws the lobby interface based on the game being played.

        :param window: Pygame surface to draw on.
        """
        self.draw_default(window)

        if self.state == "game ended":
            self.draw_game_over(window)
            return

        match self.game:
            case 'Vocabulary test':
                self.draw_generic(window)
            case 'Get 2 Zero':
                self.draw_generic(window)
            case 'Tic Tac Toe':
                self.draw_tictactoe(window)
            case 'Connect 4':
                self.draw_connect4(window)
            case 'Math Test':
                self.draw_math(window)
            case 'Guess Wrong':
                self.draw_generic(window)
            case _:  # default
                self.draw_generic(window)

    def draw_default(self, window) -> None:
        """
        Draws the default lobby interface.

        :param window: Pygame surface to draw on.
        """
        pygame.draw.rect(window, (255, 255, 255), (125, 300, 400, 400))
        pygame.draw.rect(window, (0, 0, 0), (125, 300, 400, 400), width=5)

        font = pygame.font.Font('freesansbold.ttf', 30)
        text = font.render(self.game, True, (0, 0, 0))
        text_rect = text.get_rect()
        text_rect.center = (325, 340)
        window.blit(text, text_rect)

    def draw_generic(self, window) -> None:
        """
        Draws the generic lobby interface.

        :param window: Pygame surface to draw on.
        """
        font = pygame.font.Font('freesansbold.ttf', 20)
        text = font.render('No fancy preview for the chosen game.', True, (0, 0, 0))
        text_rect = text.get_rect()
        text_rect.topleft = (140, 380)
        window.blit(text, text_rect)

        self.draw_scores(window, 450)

    def draw_tictactoe(self, window) -> None:
        """
        Draws the lobby interface for tictactoe (board).

        :param window: Pygame surface to draw on.
        """
        state = self.state.rsplit(',', 1)[0].split(',')
        cell_size = 50
        start_x = 250
        start_y = 400
        black = (0, 0, 0)

        # Draw horizontal lines
        for i in range(1, 3):
            pygame.draw.line(window, black, (start_x, start_y + i * cell_size),
                             (start_x + 150, start_y + i * cell_size), 3)

        # Draw vertical lines
        for i in range(1, 3):
            pygame.draw.line(window, black, (start_x + i * cell_size, start_y),
                             (start_x + i * cell_size, start_y + 150), 3)

        # Draw X's and O's
        for row in range(3):
            for col in range(3):
                symbol = state[row * 3 + col]
                if symbol == 'X':
                    self.draw_x(window, start_x + col * cell_size, start_y + row * cell_size, black)
                elif symbol == 'O':
                    self.draw_o(window, start_x + col * cell_size, start_y + row * cell_size, black)
                # Draw empty cell if no X or O
                elif symbol == ',':
                    self.draw_empty_cell(window, start_x + col * cell_size, start_y + row * cell_size, (255, 255, 255))

        self.draw_scores(window, 570)

    def draw_connect4(self, window) -> None:
        """
        Draws the lobby interface for connect four (board).

        :param window: Pygame surface to draw on.
        """
        state = self.state.rsplit(',', 1)[0].split(',')
        cell_size = 30
        start_x = 220
        start_y = 370
        black = (0, 0, 0)
        red = (255, 0, 0)
        blue = (0, 0, 255)

        # Draw the grid
        for row in range(6):
            for col in range(7):
                pygame.draw.rect(window, black,
                                 (start_x + col * cell_size, start_y + row * cell_size, cell_size, cell_size),
                                 2)
                symbol = state[row * 7 + col]
                if symbol == 'R':
                    self.draw_circle(window, start_x + col * cell_size + cell_size // 2,
                                     start_y + row * cell_size + cell_size // 2, red)
                elif symbol == 'B':
                    self.draw_circle(window, start_x + col * cell_size + cell_size // 2,
                                     start_y + row * cell_size + cell_size // 2, blue)

        self.draw_scores(window, 570)

    def draw_math(self, window) -> None:
        """
        Draws the lobby interface for math test (equations and their answer).

        :param window: Pygame surface to draw on.
        """
        questions = self.state.split('#')

        for index, question in enumerate(questions):
            equation, *_, answer = question.split(',')

            font = pygame.font.Font('freesansbold.ttf', 18)
            text = font.render(f'{equation} = {answer}', True, (0, 0, 0))
            text_rect = text.get_rect()
            text_rect.topleft = (140, 360 + 22 * index)
            window.blit(text, text_rect)

        self.draw_scores(window, 600)

    def draw_scores(self, window, start_y: int) -> None:
        """
        Draws the lobby interface for the scores.

        :param window: Pygame surface to draw on.
        :param start_y: The starting y to be blit on.
        """
        font = pygame.font.Font('freesansbold.ttf', 30)
        text = font.render('Scores: ', True, (0, 0, 0))
        text_rect = text.get_rect()
        text_rect.topleft = (140, start_y)
        window.blit(text, text_rect)

        font = pygame.font.Font('freesansbold.ttf', 20)

        if self.scores == 'None':
            text = font.render('no scores yet.', True, (0, 0, 0))
            text_rect = text.get_rect()
            text_rect.topleft = (140, start_y + 40)
            window.blit(text, text_rect)
            return

        for index, score in enumerate(self.scores.split(', ')):
            text = font.render(score, True, (0, 0, 0))

            text_rect = text.get_rect()
            text_rect.topleft = (140, start_y + 40 + index * 18)
            window.blit(text, text_rect)

    def draw_game_over(self, window) -> None:
        """
        Draws the lobby interface for the game over.

        :param window: Pygame surface to draw on.
        """
        font = pygame.font.Font(None, 60)
        text = font.render("Game concluded", True, (0, 0, 0))
        text_rect = text.get_rect(center=(320, 400))
        window.blit(text, text_rect)
        self.draw_scores(window, 500)

    # Function to draw a circle
    @staticmethod
    def draw_circle(window, x, y, color, radius=12) -> None:
        """
        Draws a circle on the screen.

        :param window: Pygame surface to draw on.
        :param x: X-coordinate of the center of the circle.
        :param y: Y-coordinate of the center of the circle.
        :param color: Color of the circle.
        :param radius: Radius of the circle (default is 12).
        """
        pygame.draw.circle(window, color, (x, y), radius)

    @staticmethod
    def draw_empty_cell(window, x, y, color) -> None:
        """
        Draws an empty cell on the screen.

        :param window: Pygame surface to draw on.
        :param x: X-coordinate of the center of the circle.
        :param y: Y-coordinate of the center of the circle.
        :param color: Color of the circle.
        """
        pygame.draw.rect(window, color, (x + 1, y + 1, 75 - 1, 75 - 1))

    @staticmethod
    # Function to draw X
    def draw_x(window, x, y, color) -> None:
        """
        Draws an X on the screen.

        :param window: Pygame surface to draw on.
        :param x: X-coordinate of the center of the circle.
        :param y: Y-coordinate of the center of the circle.
        :param color: Color of the circle.
        """
        pygame.draw.line(window, color, (x + 10, y + 10), (x + 50 - 10, y + 50 - 10), 3)
        pygame.draw.line(window, (0, 0, 0), (x + 50 - 10, y + 10), (x + 10, y + 50 - 10), 3)

    @staticmethod
    # Function to draw O
    def draw_o(window, x, y, color) -> None:
        """
        Draws an O on the screen.

        :param window: Pygame surface to draw on.
        :param x: X-coordinate of the center of the circle.
        :param y: Y-coordinate of the center of the circle.
        :param color: Color of the circle.
        """
        pygame.draw.circle(window, color, (x + 50 // 2, y + 50 // 2), 50 // 2 - 10, 3)


class Messages:

    def __init__(self, messages: list[str]) -> None:
        """
        Initializes a Messages object with a list of messages.

        :param messages: List of messages.
        """
        self._messages = messages

    def append_message(self, text: str) -> None:
        """
        Appends a message to the list of messages.

        :param text: Message text to append.
        """
        self._messages.append(text)

    def get_messages(self):
        """
        Returns the list of messages.

        :return: List of messages.
        """
        return self._messages

    def clear_messages(self):
        """Clears the list of messages."""
        self._messages = []


class Client:  # client object

    def __init__(self, protocol: Protocol, server_host: str, name: str) -> None:
        """
        Initializes a Client object and connects it to the server.

        :param protocol: Protocol object for communication.
        :param server_host: IP address of the server.
        :param name: Username of the client.
        """
        self.AES_KEY: None | AESCipher = None
        self.protocol: Protocol = protocol

        self.data_received: None | str = None
        self.username: str = name

        self.socket: socket.socket = socket.socket()
        self.socket.connect((server_host, protocol.PORT))
        print('connected')
        self.is_connected: bool = True
        self.exchange_keys()

    def send_request(self, request: str) -> None:
        """
        send_request sends a request to the server

        :param request: the request to send

        :return: None
        """
        enc_msg: str = self.encrypt_msg(request)

        self.socket.send(enc_msg.encode('utf-8'))

    def get_response(self, num_of_bytes: int, decode: bool = True) -> str | bytes:
        """
        get_response receives back from the server

        :return: a string of what was received
        """

        if decode:
            content: str = self.socket.recv(num_of_bytes).decode()
        else:
            content: bytes = self.socket.recv(num_of_bytes)

        # print(content)

        return content

    def close(self) -> None:
        """
        close closes the socket of the client which itself and the server communicated with.

        :return: None
        """
        self.socket.close()

    def exchange_keys(self):
        """
        Performs the key exchange process with the server.

        This method generates an AES key, exchanges public keys with the server,
        encrypts the AES key with the server's public key, and sends it to the server.
        Finally, it confirms the key exchange process with the server.

        :return: None
        """
        aes_key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))
        # print(aes_key)
        self.AES_KEY = AESCipher(aes_key)

        # Step 1
        self.socket.send('ClientHello'.encode('utf-8'))

        # Step 2
        server_hello = self.socket.recv(1024).decode()
        pub_key = self.socket.recv(1024)

        # Step 4
        self.socket.send(encrypt(RSA.import_key(pub_key), aes_key))

        # Step 5
        self.socket.send(self.AES_KEY.encrypt('Finished').encode('utf-8'))

        # Step 6
        server_finished = self.decrypt_msg(self.socket.recv(1024))

        print(f'keys exchanges, {server_hello}, {server_finished}')

    def encrypt_msg(self, msg: str) -> str:
        """
        Encrypts a message using AES encryption.

        :param msg: The message to encrypt.
        :return: The encrypted message.
        """
        # Encrypt message and add padding
        return self.AES_KEY.encrypt(msg)

    def decrypt_msg(self, enc_msg: bytes) -> str:
        """
        Decrypts a message using AES decryption.

        :param enc_msg: The encrypted message.
        :return: The decrypted message.
        """
        # Decrypt message and remove padding
        str_msg: str = enc_msg.decode('utf-8')
        return self.AES_KEY.decrypt(str_msg)

    def communicate_server(self, messages: Messages, lobbies: list[Lobby], chosen_lobby: Lobby) -> None:
        """
        Continuously communicates with the server.

        This method listens for messages from the server, decrypts them,
        processes the received data, and updates messages and lobby information.

        :param messages: Object to store and manage chat messages.
        :param lobbies: List to store available lobbies.
        :param chosen_lobby: The currently chosen lobby.
        :return: None
        """
        while self.is_connected:

            try:
                self.data_received = self.decrypt_msg(self.get_response(1024, False))

            except ConnectionAbortedError as e:
                print(f'client closed socket. {e}')
                return

            operation_number, argument = self.protocol.split_server_request(self.data_received)

            try:
                operation_number = int(operation_number)
            except TypeError as e:
                print(f'operation_number missmatch: {e}')

            print(f'{operation_number=}: {argument=}')

            if operation_number == self.protocol.SEND_TO_ADMIN:
                messages.append_message(argument)

            elif operation_number == self.protocol.GET_LOBBIES:
                self.update_operational_lobbies(lobbies, argument)

            elif operation_number == self.protocol.LOBBY_INFO:
                list_data = json.loads(argument)
                chosen_lobby.update_values(list_data)

            elif operation_number == self.protocol.ERROR_MSG:
                print(argument)

            elif operation_number == self.protocol.CLIENT_CLOSED_CONN:
                messages.append_message(argument)

    def send_text(self, text: str) -> bool:
        """
        Sends a text message to the server.

        :param text: The text message to send.
        :return: True if the message was sent successfully, False otherwise.
        """

        if len(text):
            request: str = self.protocol.build_client_request(self.protocol.SEND_TO_ADMIN, f'{self.username}: {text}')

            self.send_request(request)
            return True

        return False

    @staticmethod
    def update_operational_lobbies(lobbies: list[Lobby], argument: str) -> None:
        """
        updates the operational lobbies list.

        :param lobbies: List to store available lobbies.
        :param argument: Part of the command from the server.
        """

        lobbies.clear()

        for received_lobby in json.loads(argument):
            lobbies.append(Lobby(received_lobby[1], received_lobby[2], received_lobby[3],
                                 received_lobby[4], received_lobby[0]))


def draw_window(window: pygame.Surface, buttons: list[Button], username: str, text: str,
                messages: Messages, lobbies: list[Lobby], lobby_offset: int, is_chosen: bool,
                chosen_lobby: Lobby) -> None:
    """
    Draws the main game window.

    :param window: The Pygame surface representing the game window.
    :param buttons: List of buttons to be drawn.
    :param username: The username of the player.
    :param text: The text entered by the player.
    :param messages: Object to store and manage chat messages.
    :param lobbies: List of available lobbies.
    :param lobby_offset: Offset for displaying lobby previews.
    :param is_chosen: Flag indicating if a lobby is chosen.
    :param chosen_lobby: The lobby chosen by the player.
    """

    # colors
    black: tuple[int, int, int] = (0, 0, 0)
    white: tuple[int, int, int] = (255, 255, 255)
    background_color: tuple[int, int, int] = (127, 127, 127)

    window.fill(background_color)

    if not is_chosen:

        if not len(lobbies):
            font = pygame.font.Font('freesansbold.ttf', 40)
            text_surface = font.render(f"No open lobbies", True, black)
            text_rect = text_surface.get_rect(topleft=(125, 350))
            window.blit(text_surface, text_rect)

        # render lobby view!
        offset = lobby_offset + 300
        for lobby in lobbies:
            lobby.draw_preview(window, color=(0, 0, 0), position=(125, offset), width=400, height=60,
                               text_pos=(140, offset + 10), size=20)
            offset += 100

        lobby_restricted_zone = pygame.Rect(100, 0, 500, 300)
        pygame.draw.rect(window, background_color, lobby_restricted_zone)
        lobby_restricted_zone = pygame.Rect(100, 700, 500, 300)
        pygame.draw.rect(window, background_color, lobby_restricted_zone)
    else:
        if chosen_lobby:
            chosen_lobby.draw_lobby(window)

        # draws the buttons
        for but in buttons:
            if but.optional:
                but.draw_button()

    # draws the buttons
    for but in buttons:
        if not but.optional:
            but.draw_button()

    chat_box = pygame.Rect(600, 100, 1200, 800)
    pygame.draw.rect(window, white, chat_box)
    pygame.draw.rect(window, black, chat_box, 7)

    entry_box = pygame.Rect(760, 120, 900, 30)

    pygame.draw.rect(window, background_color, entry_box)
    pygame.draw.rect(window, black, entry_box, 4)
    pygame.draw.rect(window, black, (600, 160, 1200, 7))

    # Render the text
    font = pygame.font.Font('freesansbold.ttf', 20)
    text_surface = font.render(f"Enter Text:  {text}", True, black)
    text_rect = text_surface.get_rect(topleft=(650, 125))
    window.blit(text_surface, text_rect)

    text_surface = font.render(f"Hello, {username}", True, black)
    text_rect = text_surface.get_rect(topleft=(260, 250))
    window.blit(text_surface, text_rect)

    text_surface = font.render(f"Logs:", True, black)
    text_rect = text_surface.get_rect(topleft=(650, 180))
    window.blit(text_surface, text_rect)

    for i, msg in enumerate(messages.get_messages()):
        text_surface = font.render(msg, True, black)
        text_rect = text_surface.get_rect(topleft=(650, 220 + 30 * i))
        window.blit(text_surface, text_rect)


def quit_game(exit_client: Client) -> None:
    """
    Notifies the server before exiting the game.

    :param exit_client: The client object used to communicate with the server.
    :return: None
    """
    exit_req: str = exit_client.protocol.build_client_request(exit_client.protocol.CLOSE_CONNECTION)
    exit_client.send_request(exit_req)
    exit_client.close()
    quit()


def get_lobbies(client: Client, protocol: Protocol, username: str) -> None:
    """
    Requests the list of available lobbies from the server.

    :param client: The client object used to communicate with the server.
    :param protocol: The protocol object defining communication protocols.
    :param username: The username of the player.
    :return: None
    """
    request = protocol.build_client_request(protocol.GET_LOBBIES, username, '0')

    client.send_request(request)


def choose_lobby(protocol: Protocol, client: Client, username: str) -> tuple[bool, Lobby]:
    """
    Requests to reset the chosen lobby

    :param client: The client object used to communicate with the server.
    :param protocol: The protocol object defining communication protocols.
    :param username: The username of the player.
    :return: tuple of False and a placeholder lobby
    """
    request = protocol.build_client_request(protocol.LOBBY_INFO, username, str(0))
    client.send_request(request)
    return False, Lobby(placeholder=True)


def validate_username(username):
    """
    Validate a username.

    :param username: The username to validate (str).
    :return: True if the username is valid (not empty and consists only of English
             characters, numbers, and spaces), False otherwise.
    """
    # Check if the username is not empty
    if not username or len(username) > 30:
        return False

    # Check if the username consists only of English characters, numbers, and spaces
    if not re.match(r'^[a-zA-Z0-9\s]+$', username):
        print('username cant have any special characters')
        return False

    return True


def main():
    pygame.init()

    username: str = ''

    while not validate_username(username):
        username = input("Enter username: ")

    width, height = 1920, 1020
    window: pygame.Surface = pygame.display.set_mode((width, height))

    protocol: Protocol = Protocol(user_type='client')

    messages = Messages([])
    lobbies = []

    client = Client(protocol, protocol.SERVER_IP, username)

    # Creates all the buttons
    buttons = [Button(window, (0, 0, 0), (200, 100), 250, 100, 'Send', 320, 150, 50, 0),
               Button(window, (0, 0, 0), (100, 225), 100, 50, 'Back', 150, 250, 20, 6, True),
               Button(window, (0, 0, 0), (225, 725), 180, 50, 'Refresh lobbies', 315, 750, 20, 7),
               Button(window, (0, 0, 0), (200, 800), 250, 100, 'Clear logs', 320, 850, 40, 8)]

    run: bool = True
    text: str = ""
    lobby_offset: int = 0
    is_chosen = False
    chosen_lobby: Lobby = Lobby(placeholder=True)

    clock = pygame.time.Clock()

    thread = threading.Thread(target=client.communicate_server, args=[messages, lobbies, chosen_lobby])
    thread.start()

    # gets initial lobbies
    get_lobbies(client, protocol, username)

    while run:

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                quit_game(client)

            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:  # Scroll up
                    lobby_offset -= 10
                elif event.button == 5:  # Scroll down
                    if not lobby_offset >= 0:
                        lobby_offset += 10

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if client.send_text(text):
                        messages.append_message(f"{username}: {text}")
                        text = ''
                elif event.key == pygame.K_BACKSPACE:
                    text = text[:-1]
                else:
                    text += event.unicode

            elif event.type == pygame.MOUSEBUTTONUP and 5 > event.button < 4:
                mouse_x, mouse_y = pygame.mouse.get_pos()

                for lobby in lobbies:
                    if (not is_chosen) and ((cur_lobby := lobby.is_clicked(mouse_x, mouse_y)) is not None):
                        request = protocol.build_client_request(protocol.LOBBY_INFO, username, str(cur_lobby.id))
                        client.send_request(request)
                        chosen_lobby.copy_lobby(cur_lobby)
                        is_chosen = True

                for but in buttons:
                    if but.is_clicked(mouse_x, mouse_y):
                        if 'Send' in but.string:
                            if client.send_text(text):
                                messages.append_message(f"{username}: {text}")
                                text = ''
                        elif 'Clear logs' in but.string:
                            messages.clear_messages()
                        elif 'Refresh' in but.string:
                            get_lobbies(client, protocol, username)
                        elif 'Back' in but.string:
                            is_chosen, chosen_lobby = choose_lobby(protocol, client, username)

        draw_window(window, buttons, username, text, messages,
                    lobbies, lobby_offset, is_chosen, chosen_lobby)
        pygame.display.flip()

        clock.tick(protocol.FPS)

    pygame.quit()


if __name__ == '__main__':
    # Ensures the main function is executed when the script is run directly
    main()
