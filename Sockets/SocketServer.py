import socket
import threading

import requests
from AES import *
from RSA import *

import pygame

from Protocol import Protocol


class Server:  # server object
    def __init__(self, host: str, port: int) -> None:
        """
        init creates a new server who will interact with the clients

        :param host: ip of the server (0.0.0.0)
        :param port: port of the server

        :return: None
        """
        self.host: str = host
        self.port: int = port
        self.socket: socket.socket = self.init_server()

    def init_server(self) -> socket.socket:
        """
        init_server creates a new server socket who will interact with the clients and starts to listen

        :return: socket object for the server
        """
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((self.host, self.port))
        server_socket.listen(2)  # connects up to 2 clients (in theory, in practice the os dictates how many can join.)

        return server_socket

    def connect_client(self) -> tuple[socket.socket, str]:
        """
        connect_client connects to new client

        :return: tuple consisting of the client socket, and the client ip
        """

        client_socket, client_address = self.socket.accept()

        return client_socket, client_address

    def close(self) -> None:
        """
        close closes the socket of the server

        :return: None
        """
        self.socket.close()

    def send(self, client_id: int, msg: str) -> None:
        """
        send data to a client

        :param client_id:
        :param msg: the data to send the client (str)

        :return: None
        """

        enc_msg: str = self.encrypt_message(msg, client_id)

        lock.acquire()
        clients[client_id].socket.send(enc_msg.encode('utf-8'))
        lock.release()

    def handle_get_lobbies(self, client_id: int) -> None:
        """
        Handles the request to get the list of operational lobbies and sends
         the response to the client.

        :param client_id: The ID of the client making the request.
        :return: None
        """
        response = protocol.build_server_request(protocol.GET_LOBBIES, get_operational_lobbies())

        self.send(client_id, response)

    def handle_lobby_info(self, client_id: int, par1: str, par2: str) -> None:
        """
        Handles the request to get information about a specific lobby
         or to unselect a lobby.

        :param client_id: The ID of the client making the request.
        :param par1: The name of the client making the request.
        :param par2: The ID of the lobby being requested or unselected (0 for unselecting).
        :return: None
        """
        lock.acquire()

        if int(par2) == 0:
            clients[client_id].active_lobby = False
            clients[client_id].request = None
            lock.release()

            notification = protocol.build_server_request(protocol.SEND_TO_ADMIN,
                                                         f'{par1} unselected the lobby.')
            send_to_other(self, client_id, notification)

            return

        users: str = get_users_in_lobby(int(par2))
        clients[client_id].active_lobby = True
        clients[client_id].choose_lobby(self, client_id, int(par2))

        if users == 'no lobby found':
            print(users)  # 'no lobby found'
            clients[client_id].request = None
            lock.release()
            return

        lock.release()

        notification = protocol.build_server_request(protocol.SEND_TO_ADMIN,
                                                     f'{par1} chose lobby with users: {users}')
        send_to_other(self, client_id, notification)

    def handle_close_connection(self, client_id: int) -> None:
        """
        Handles the request to close the connection for a specific client.

        :param client_id: The ID of the client whose connection is to be closed.
        :return: None
        """
        print('closing connection')
        notification = protocol.build_server_request(protocol.CLIENT_CLOSED_CONN, f'other client left.')

        send_to_other(self, client_id, notification, False)

        lock.acquire()
        clients[client_id].socket.close()
        clients[client_id] = ClientInfo(placeholder=True)
        lock.release()

        return

    def handle_send_to_admin(self, client_id: int, par1: str, par2: str):
        """
        Handles the request to send a notification to the admin.

        :param client_id: The ID of the client maתking the request.
        :param par1: The first part of the message to be sent.
        :param par2: The second part of the message to be sent.
        :return: None
        """
        notification = protocol.build_server_request(protocol.SEND_TO_ADMIN, f'{par1} {par2}')

        send_to_other(self, client_id, notification)

    @staticmethod
    def receive(client_socket: socket.socket, num_of_bytes: int = 1024, decode: bool = True) -> str | bytes:
        """
        get_response receives back from the server

        :return: a string of what was received
        """

        if decode:
            content: str = client_socket.recv(num_of_bytes).decode()
        else:
            content: bytes = client_socket.recv(num_of_bytes)

        return content

    def execute_key_exchange(self, client_socket: socket.socket) -> None:
        """
        Executes the key exchange process with the client.

        :param client_socket: Socket of the client
        :return: None
        """

        pub_key, pri_key = get_keys(1024)

        # Step 1
        client_hello = client_socket.recv(1024).decode('utf-8')

        # Step 2
        client_socket.send('ServerHello'.encode('utf-8'))
        client_socket.send(pub_key.export_key())

        # Step 4
        enc_aes_key = client_socket.recv(1024)
        aes_key = decrypt(pri_key, enc_aes_key).decode('utf-8')

        for index, client in enumerate(clients):
            if client.socket == client_socket:
                clients[index].key = AESCipher(aes_key)

                # Step 5

                client_finished = self.decrypt_message(client_socket.recv(1024), index)

                # Step 6
                client_socket.send(clients[index].key.encrypt('Finished').encode('utf-8'))

                print(f'keys exchanges, {client_hello}, {client_finished}')

                return

    @staticmethod
    def encrypt_message(msg: str, client_id: int) -> str:
        """
        Encrypts a message for a client.

        :param msg: Message to encrypt
        :param client_id: ID of the client
        :return: Encrypted message (str)
        """

        # Encrypt message and add padding
        lock.acquire()
        enc_msg = clients[client_id].key.encrypt(msg)
        lock.release()
        return enc_msg

    @staticmethod
    def decrypt_message(enc_msg: bytes, client_id: int) -> str:
        """
        Decrypts a message from a client.

        :param enc_msg: Encrypted message (bytes)
        :param client_id: ID of the client
        :return: Decrypted message (str)
        """

        # Decrypt message and remove padding
        lock.acquire()
        dec_msg = clients[client_id].key.decrypt(enc_msg.decode('utf-8'))
        lock.release()
        return dec_msg


class ClientInfo:

    def __init__(self, client_socket: socket.socket = None, placeholder: bool = False) -> None:
        """
        Initializes client information.

        :param client_socket: Socket of the client
        :param placeholder: Whether this is a placeholder client
        """

        self.placeholder = placeholder
        if placeholder:
            return

        self.socket: socket.socket = client_socket
        self.request = None
        self.key: AESCipher | None = None
        self.active_lobby: bool = False

    def choose_lobby(self, server: Server, client_id: int, lobby_id: int) -> None:
        """
        Starts a thread for communicating within a lobby.

        :param server: Server instance
        :param client_id: ID of the client
        :param lobby_id: ID of the lobby
        :return: None
        """
        threading.Thread(target=self.communicate_lobby, args=[server, client_id, lobby_id]).start()

    def communicate_lobby(self, server: Server, client_id: int, lobby_id: int) -> None:
        """
        Communicates and get updates from the lobby.

        :param server: Server instance
        :param client_id: ID of the client
        :param lobby_id: ID of the lobby
        :return: None
        """

        cur_lobby: str | None = None

        while self.active_lobby:
            lobby = requests.get(f'http://{protocol.http_server_ip}:{protocol.http_server_port}/getLobbyData',
                                 params={'lobby_id': lobby_id}).text

            if lobby != cur_lobby:

                response = protocol.build_server_request(protocol.LOBBY_INFO, lobby)

                server.send(client_id, response)

            cur_lobby = lobby


def handle_client_input(server: Server, client_id: int) -> None:
    """
    Handles client input.

    :param server: Server instance
    :param client_id: ID of the client
    :return: None
    """
    global clients

    while not clients[client_id].placeholder:
        try:
            request: bytes = server.receive(clients[client_id].socket, decode=False)

            clients[client_id].request = server.decrypt_message(request, client_id)

            operation_number: int = int(protocol.split_client_request(clients[client_id].request)[0])

            if operation_number == protocol.CLOSE_CONNECTION:
                return  # allows handle client finish up the connection.
        except ConnectionError as e:
            print(f'client socket closed. {e}')
            clients[client_id] = ClientInfo(placeholder=True)


def handle_client(server: Server, client_id: int) -> None:
    """
    Handles client requests.

    :param server: Server instance
    :param client_id: ID of the client
    :return: None
    """
    while not clients[client_id].placeholder:

        clock.tick(protocol.FPS)

        # receive the request from the client
        try:
            request = clients[client_id].request
        except AttributeError:
            return

        if request is not None:

            # split up the request and compare it:
            operation_number, par1, par2 = protocol.split_client_request(request)

            print(f'{client_id = }: {operation_number = }, {par1 = }, {par2 = }\n')

            match int(operation_number):
                case protocol.SEND_TO_ADMIN:
                    server.handle_send_to_admin(client_id, par1, par2)

                case protocol.GET_LOBBIES:
                    server.handle_get_lobbies(client_id)

                case protocol.LOBBY_INFO:
                    server.handle_lobby_info(client_id, par1, par2)

                case protocol.CLOSE_CONNECTION:
                    server.handle_close_connection(client_id)

                case _:
                    print(f'Unknown operation {operation_number}')

            clients[client_id].request = None


def start_connecting_clients(server: Server) -> None:
    """
    Creates a thread with target connect_clients_thread and starts it.

    :param server: The main server
    :return: None
    """

    thread: threading.Thread = threading.Thread(target=connect_clients_thread, args=[server])
    thread.start()


def connect_clients_thread(server: Server) -> None:
    """
    Continuously waits and receives new clients, creating a thread for each.
    Able to connect up to 2 clients simultaneously

    :param server: The main server
    :return: None
    """

    while True:
        if clients[0].placeholder or clients[1].placeholder:
            client_socket, client_address = server.connect_client()
            create_new_client_thread(server, client_socket)

            print(f'new client from ip: {client_address}')


def create_new_client_thread(server: Server, client_socket: socket.socket) -> None:
    """
    Creates new threads for handling client requests and input.

    :param server: The main server
    :param client_socket: The current client socket
    :return: None
    """
    global clients

    client_id = set_client(client_socket)

    server.execute_key_exchange(client_socket)

    thread = threading.Thread(target=handle_client, args=[server, client_id])
    thread.start()

    thread2 = threading.Thread(target=handle_client_input, args=[server, client_id])
    thread2.start()

    client_id += 1

    # used to keep track of client's threads
    threads.append((thread, thread2))


def set_client(client_socket: socket.socket) -> int:
    """
    Sets a new client in the clients list.

    :param client_socket: The client socket
    :return: The client ID
    """
    if clients[0].placeholder:
        clients[0] = ClientInfo(client_socket)
        return 0
    elif clients[1].placeholder:
        clients[1] = ClientInfo(client_socket)
        return 1
    else:
        raise Exception("Unexpected client, report this..")


def get_other_id(id: int):
    """
    Gets the ID of the other client.

    :param id: The current client ID
    :return: The other client ID
    """
    if id == 1:
        return 0
    return 1


def send_to_other(server: Server, id: int, response: str, responding: bool = True) -> None:
    """
    Sends a message to the other client.

    :param server: The server instance
    :param id: The current client ID
    :param response: The response message
    :param responding: Whether a response is expected
    :return: None
    """
    other_id = get_other_id(id)

    if not clients[other_id].placeholder:
        server.send(other_id, response)  # send to other user.
    else:
        if responding:
            # notify client
            server.send(id, protocol.build_error_msg("No other user connected.."))


def get_users_in_lobby(id: int) -> str:
    """
    Gets the users in a lobby.

    :param id: The lobby ID
    :return: Users in the lobby (str)
    """
    lobby = requests.get(f'http://{protocol.http_server_ip}:{protocol.http_server_port}/getLobbyUsers',
                         params={'id': id})
    return lobby.text


def get_operational_lobbies() -> str:
    """
    Gets the operational lobbies.

    :return: Operational lobbies (str)
    """
    lobby = requests.get(f'http://{protocol.http_server_ip}:{protocol.http_server_port}/getOperationalLobbies')

    return lobby.text


lock = threading.Lock()

threads: list = []

current_client_id: int = 0

clients = [ClientInfo(placeholder=True), ClientInfo(placeholder=True)]
pygame.init()

clock: pygame.time.Clock = pygame.time.Clock()

protocol: Protocol = Protocol('server')


def main() -> None:
    """Main function to create and start the server."""
    # creates the main server
    server: Server = Server(host="0.0.0.0", port=protocol.PORT)

    print(f"Server is up up and running!")
    print(f"listing on 0.0.0.0 at port {protocol.PORT}")

    # starts the threading and listens to clients connections
    start_connecting_clients(server)


if __name__ == '__main__':
    # Ensures the main function is executed when the script is run directly
    main()
