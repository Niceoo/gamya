import pygame


class Button:

    def __init__(self, new_window: pygame.Surface, color: tuple[int, int, int], position: tuple[int, int], width: int,
                 height: int, string: str, string_x: int, string_y: int, size: int, id: int,
                 optional: bool = False) -> None:
        """
        init creates a new button on which a text is written

        :param: new_window: the window onto the button is drawn
        :param color: the color of the button
        :param position: the position of the top-left
        :param width: the length of the button from the starting pos
        :param height: the height of the button from the starting pos
        :param string: the string written inside(preferably) of the button
        :param string_x: the string x
        :param string_y: the string y

        :return: None
        """

        self.color = color
        self.position = position
        self.width = width
        self.height = height
        self.string = string
        self.window = new_window
        self.size = size
        self.id = id
        self.optional = optional

        # pygame.display.set_caption('Show Text')
        self.font = pygame.font.Font('freesansbold.ttf', self.size)
        self.text = self.font.render(string, True, color)
        self.textRect = self.text.get_rect()
        self.textRect.center = (string_x, string_y)

    def draw_button(self) -> None:
        """
        draws (blit) the button onto the screen

        :return: None
        """
        pygame.draw.rect(self.window, (255, 255, 255), (self.position, (self.width, self.height)))
        pygame.draw.rect(self.window, self.color, (self.position, (self.width, self.height)), width=5)

        self.window.blit(self.text, self.textRect)

    def is_clicked(self, mouse_x, mouse_y) -> bool:
        """
        checks if the button is pressed

        :return: boolean value if the mouse x,y are in the button
        """

        if (self.position[0] <= mouse_x <= self.position[0] + self.width) and (
                self.position[1] <= mouse_y <= self.position[1] + self.height):
            return True

        return False
