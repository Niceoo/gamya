# the port the client is going to send requests to server

class Protocol:
    # port on which the socket server runs.
    PORT: int = 8820

    # Updates per second (for clients)
    FPS: int = 60

    def __init__(self, user_type: str) -> None:
        """
        init creates a new protocol who will interact with the clients and server (differently).

        :param user_type: The type of the user, server/client

        :return: None
        """

        # clients requests:
        self.ERROR_MSG: int = 9
        self.SEND_TO_ADMIN: int = 1
        self.GET_LOBBIES: int = 3
        self.LOBBY_INFO: int = 4
        self.CLOSE_CONNECTION: int = 6
        self.CLIENT_CLOSED_CONN: int = 8

        if user_type == 'client':
            # the server's ip
            self.SERVER_IP: str = "192.168.1.181"
        elif user_type == 'server':
            self.http_server_ip = "192.168.1.181"
            self.http_server_port = "5000"

    @staticmethod
    def split_client_request(request: str) -> tuple[str, str, str] | tuple[None, None, None]:

        """
        split_client_request splits the request into OPERATION_NUMBER, ARG1, ARG2.

        :param request: The request (string) to be split up.

        :return: Tuple of None if the request is invalid
                 or tuple of operation number and arguments if valid.
        """

        if len(request) < 2:
            raise Exception(f"Invalid request: {request}")

        operation_number: str = request[0]
        arguments = request[2:].split("/")

        if len(arguments) != 2:
            raise Exception(f"Invalid request: {request}")

        arg1, arg2 = arguments
        return operation_number, arg1, arg2

    @staticmethod
    def split_server_request(request: str) -> tuple[str, str] | str:
        """
        split_request splits the request into OPERATION_NUMBER, ARG.

        :param request: The request (string) to be split up.

        :return: Raise exception if invalid request
                 or tuple of operation number and argument if valid.
        """

        operation_number: str = request[0]
        argument = request[2:]  # split['#']

        if not argument:
            raise Exception(f'invalid request {request}')

        return operation_number, argument

    @staticmethod
    def build_client_request(operation_number: int, arg1: str = "", arg2: str = "") -> str:
        """
        builds the client's request

        :param operation_number: the number of the operation
        :param arg1: the first argument
        :param arg2: the second argument

        :return: the new command(operation)

        # used, clients requests:
        self.SEND_TO_ADMIN: message for other admin
        self.GET_LOBBIES: request for all active lobbies
        self.LOBBY_INFO: lobby id of the lobby chosen
        self.CLOSE_CONNECTION: notify end of connection

        """

        built_req: str = f"{operation_number}#{arg1}/{arg2}"
        # print(f 'built req: {built_req} ')

        return f"{operation_number}#{arg1}/{arg2}"

    @staticmethod
    def build_server_request(operation_number: int, arg1: str = "") -> str:
        """
        builds the server's request

        :param operation_number: the number of the operation
        :param arg1: the first argument

        :return: the new command(operation)

        # used, server requests/responses:
        self.ERROR_MSG: Admin sent a message to a non-existing other.
        self.SEND_TO_ADMIN: message to be displayed in logs.
        self.GET_LOBBIES: all active lobbies
        self.LOBBY_INFO: lobby data of specific lobby
        self.CLIENT_CLOSED_CONN: notification to other clients that the other left.
        """

        built_req: str = f"{operation_number}#{arg1}"
        # print(f 'built req: {built_req} ')

        return f"{built_req}"

    def build_error_msg(self, detail: str) -> str:
        """
        Build an error message.

        :param detail: The detail of the error.

        :return: The formatted error message.
        """
        return f"{self.ERROR_MSG}#{detail}"
