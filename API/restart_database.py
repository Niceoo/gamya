import sqlite3
from pathlib import Path
from API.global_funcs import pickle_new_generator


def delete_database() -> None:
    """
    Deletes the SQLite database file if it exists.
    """
    import os

    # Get the absolute path to the database file
    file_path = Path(__file__).resolve().parent / "data.db"

    # Check if the file exists before attempting to delete it
    if os.path.exists(file_path):
        # Delete the file
        os.remove(file_path)
        print(f"File '{file_path}' deleted successfully.")
    else:
        print(f"The file '{file_path}' does not exist.\nso it wasn't deleted.")


def create_tables() -> None:
    """
    Creates the required database tables.
    """
    create_user_table()
    create_party_table()
    create_lobby_table()


def create_party_table() -> None:
    """
    Creates the 'parties' table in the database if it doesn't exist.
    """
    conn: sqlite3.Connection = sqlite3.connect('data.db')
    cursor: sqlite3.Cursor = conn.cursor()

    cursor.execute('''
            CREATE TABLE IF NOT EXISTS parties (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                leader TEXT NOT NULL,
                users TEXT NOT NULL
            )
        ''')

    conn.commit()
    cursor.close()
    conn.close()

    print('created table parties')


def create_lobby_table() -> None:
    """
    Creates the 'lobbies' table in the database if it doesn't exist.
    """
    conn: sqlite3.Connection = sqlite3.connect('data.db')
    cursor: sqlite3.Cursor = conn.cursor()

    cursor.execute('''
        CREATE TABLE IF NOT EXISTS lobbies (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            users TEXT NOT NULL,
            game TEXT NOT NULL,
            scores TEXT NOT NULL,
            state TEXT NOT NULL
        )
    ''')

    conn.commit()
    cursor.close()
    conn.close()

    print('created table lobbies')


def create_user_table() -> None:
    """
    Creates the 'users' table in the database if it doesn't exist.
    """
    conn: sqlite3.Connection = sqlite3.connect('data.db')
    cursor: sqlite3.Cursor = conn.cursor()

    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            email TEXT NOT NULL,
            birthday DATE NOT NULL,
            password TEXT NOT NULL,
            online INTEGER
        )
    ''')

    conn.commit()
    cursor.close()
    conn.close()

    print('created table users')


def main():
    # uncomment to creates a new id generator:
    # (if u create a new id gen, you have to clear users or get last id as the start of the new gen

    last_index: int = 0
    pickle_new_generator(last_index)

    # uncomment this to reset database including id:
    delete_database()

    create_tables()


if __name__ == '__main__':
    main()
