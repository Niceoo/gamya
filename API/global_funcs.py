import pickle
import secrets
from datetime import datetime, timedelta
from typing import Optional

import jwt
from fastapi import Header, HTTPException

from global_databse_funcs import *


def remove_online(username_to_remove: str) -> None:
    """
    Removes a user from the online list and updates their status in the database.

    Args:
        username_to_remove (str): The username of the user to remove.
    """
    try:
        client_req.pop(username_to_remove)
    except KeyError as e:
        print(f'Somehow user is already offline {e}')
    update_user(username_to_remove, key='online', change='0')

    print(f"Removed {username_to_remove}, updated req: {client_req}")


def re_add_online(username_to_add: str) -> None:
    """
    Re-adds a user to the online list and updates their status in the database.

    Args:
        username_to_add (str): The username of the user to re-add.
    """
    client_req.pop(username_to_add)
    update_user(username_to_add, key='online', change='1')

    print(f"Added back {username_to_add}, updated req: {client_req}")


def get_update_json(update_data: any) -> dict[str, any]:
    """
    Creates a JSON structure for an update.

    Args:
        update_data (any): The data to include in the update.

    Returns:
        dict[str, any]: A dictionary containing the update information.
    """
    data: dict[str, any] = {
        "isUpdate": 'true',
        "data": update_data
    }
    return data


class IDGenerator:
    def __init__(self, start: int = 0) -> None:
        """
        Initializes the IDGenerator with a starting ID.

        Args:
            start (int, optional): The starting ID. Defaults to 0.
        """
        self.current_id: int = start

    def generate_id(self) -> int:
        """
        Generates a new ID.

        Returns:
            int: The newly generated ID.
        """
        self.current_id += 1
        return self.current_id

    def update(self, current_id: dict) -> None:
        """
        Updates the current ID with the given ID.

        Args:
            current_id (dict): A dictionary containing the current ID.
        """
        self.__dict__.update(current_id)


def pickle_new_generator(start_index: int = 0) -> None:
    """
    Creates or resets the ID generator and saves it to a pickle file.

    Args:
        start_index (int, optional): The starting index for the ID generator. Defaults to 0.
    """
    with open('current_user_id.pickle', 'wb') as file:
        pickle.dump(IDGenerator(start_index).__dict__, file)

    print('current_user_id created (or reset)')


def read_pickle(filepath: str) -> any:
    """
    Reads an object from a pickle file.

    Args:
        filepath (str): The path to the pickle file.

    Returns:
        any: The object read from the pickle file.
    """
    with open(filepath, 'rb') as file:
        return pickle.load(file)


def write_pickle(filepath: str, obj: any) -> None:
    """
    Writes an object to a pickle file.

    Args:
        filepath (str): The path to the pickle file.
        obj (any): The object to write to the pickle file.
    """
    with open(filepath, 'wb') as file:
        pickle.dump(obj, file)


def get_new_user_id() -> int:
    """
    Generates a new user ID.

    Returns:
        int: The newly generated user ID.
    """
    current_id: dict = read_pickle('current_user_id.pickle')

    id_gen: IDGenerator = IDGenerator()
    id_gen.update(current_id)

    new_id: int = id_gen.generate_id()

    write_pickle('current_user_id.pickle', id_gen.__dict__)

    return new_id


def add_party_member(leader: str, to_add: str) -> None:
    """
    Adds a member to a party. If the party does not exist, it creates a new one.

    Args:
        leader (str): The username of the party leader.
        to_add (str): The username of the member to add.
    """
    exists, _ = search_party_by_leader(leader)

    if exists:
        append_party(leader, to_add)
    else:
        add_party(leader, to_add)


def generate_access_token(user_id: int, username: str, expires_in_minutes: int = 3600) -> str:
    """
    Generates a JWT access token.

    Args:
        user_id (int): The user ID.
        username (str): The username.
        expires_in_minutes (int, optional): Token expiration time in minutes. Defaults to 3600.

    Returns:
        str: The generated JWT access token.
    """
    payload = {
        "user_id": user_id,
        "username": username,
        "exp": datetime.utcnow() + timedelta(minutes=expires_in_minutes)
    }
    gen_access_token = jwt.encode(payload, SECRET_KEY, algorithm=ALGORITHM)

    return gen_access_token


def verify_token(authorization: Optional[str] = Header(None)):
    """
    Verifies the JWT token from the authorization header.

    Args:
        authorization (Optional[str], optional): The authorization header. Defaults to None.

    Raises:
        HTTPException: If the token is missing or invalid.

    Returns:
        dict: The payload of the verified token.
    """
    if authorization is None:
        raise HTTPException(status_code=401, detail="Authorization header missing")
    try:
        token = authorization.split()[1]
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return payload
    except Exception as e:
        print(e)
        raise HTTPException(status_code=401, detail="Invalid token")


def fetch_all_usernames() -> list[str]:
    """
    Fetches all usernames from the database.

    Returns:
        list[str]: A list of usernames.
    """
    data = fetch_all_users()

    return [row["username"] for row in data]


def fetch_all_online() -> list[str]:
    """
    Fetches all online users from the database.

    Returns:
        list[str]: A list of online usernames.
    """
    data = fetch_all_users()

    online_users = []

    for row in data:
        if row['online'] == 1:
            online_users.append(row['username'])

    return online_users


def fetch_all_data_tuple() -> tuple[list, list, list]:
    """
    Fetches all users, parties, and lobbies from the database.

    Returns:
        tuple[list, list, list]: A tuple containing lists of users, parties, and lobbies.
    """
    users = fetch_all_users_tuple()
    parties = fetch_all_parties_tuple()
    lobbies = fetch_all_lobbies_tuple()

    return users, parties, lobbies


def fetch_lobby_state(lobby_id: int) -> str:
    """
    Fetches the state of a lobby by its ID.

    Args:
        lobby_id (int): The ID of the lobby.

    Returns:
        str: The state of the lobby.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    return lobby['state']


def count_users_in_lobby(lobby_id: int) -> int:
    """
    Counts the number of users in a lobby.

    Args:
        lobby_id (int): The ID of the lobby.

    Returns:
        int: The number of users in the lobby.
    """
    return len(get_user_in_lobby(lobby_id))


# Generate a random secret key
SECRET_KEY: str = secrets.token_hex(32)
ALGORITHM: str = "HS256"

client_req = {username: {} for username in fetch_all_usernames()}
