import sqlite3
import threading


def fetch_all_users() -> list[dict[str, any]]:
    """
    Fetches all users from the database.

    Returns:
        list[dict[str, any]]: A list of dictionaries containing user information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users')
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return [{'username': row[1], 'email': row[2], 'birthday': row[3], 'password': row[4],
             'online': row[5], 'id': row[0]} for row in rows]


def search_party_by_leader(leader: str) -> tuple[bool, str]:
    """
    Searches for a party by the leader's username.

    Args:
        leader (str): The username of the party leader.

    Returns:
        tuple[bool, str]: A tuple containing a boolean indicating if the party was found, and the members if found.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM parties WHERE leader = ?', (leader,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        print(f'Party found: {row}')
        return True, row[2]  # Return members

    return False, 'None'


def add_party(leader: str, users: str) -> None:
    """
    Adds a new party to the database.

    Args:
        leader (str): The username of the party leader.
        users (str): A string of comma-separated usernames of party members.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute(
        'INSERT INTO parties (leader, users) VALUES (?, ?)', (leader, users,)
    )
    conn.commit()
    cursor.close()
    conn.close()

    print(f'Party led by: {leader}, inserted')


def append_party(leader: str, to_add: str) -> None:
    """
    Appends a user to an existing party.

    Args:
        leader (str): The username of the party leader.
        to_add (str): The username to add to the party.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM parties WHERE leader = ?', (leader,))
    members = cursor.fetchone()[2]  # Get current members
    conn.commit()

    to_add = ', '.join([members, to_add])

    print(f"Added: {to_add}")

    cursor.execute(
        'UPDATE parties SET users = ? WHERE leader = ?',
        (to_add, leader,)
    )
    conn.commit()
    cursor.close()
    conn.close()


def scan_user(username: str) -> tuple[bool, int, str]:
    """
    Scans for a user by username.

    Args:
        username (str): The username to scan for.

    Returns:
        tuple[bool, int, bytes]: A tuple containing a boolean indicating if the user was found,
                                  the user ID, and the password.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE username = ?', (username,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        return True, row[0], row[4]  # Return user ID and password

    return False, -1, '-1'


def fetch_all_users_tuple() -> list:
    """
    Fetches all users from the database as tuples.

    Returns:
        list: A list of tuples containing user information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users')
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def fetch_all_parties_tuple() -> list:
    """
    Fetches all parties from the database as tuples.

    Returns:
        list: A list of tuples containing party information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM parties')
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def fetch_all_lobbies_tuple() -> list:
    """
    Fetches all lobbies from the database as tuples.

    Returns:
        list: A list of tuples containing lobby information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM lobbies')
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def insert_user(id, username: str, email: str, birthday: str, password: str) -> None:
    """
    Inserts a new user into the database.

    Args:
        id (int): The user's ID.
        username (str): The username of the user.
        email (str): The email of the user.
        birthday (str): The birthday of the user.
        password (str): The password of the user.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute(
        'INSERT INTO users (id, username, email, birthday, password, online) VALUES (?, ?, ?, ?, ?, 1)',
        (id, username, email, birthday, password)
    )
    conn.commit()
    cursor.close()
    conn.close()

    print('User inserted')


def update_user(username: str, change: str, key: str) -> None:
    """
    Updates a user's information in the database.

    Args:
        username (str): The username of the user.
        change (str): The new value for the specified key.
        key (str): The key (column) to update.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    query = f'UPDATE users SET {key} = ? WHERE username = ?'
    cursor.execute(query, (change, username))
    conn.commit()
    cursor.close()
    conn.close()


def delete_user(id: int) -> None:
    """
    Deletes a user from the database.

    Args:
        id (int): The ID of the user to delete.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('DELETE FROM users WHERE id = ?', (id,))
    conn.commit()
    cursor.close()
    conn.close()
    print(f'deleted {id}')


def add_lobby(users_to_add: str, game: str, state: str = "None") -> int:
    """
    Adds a new lobby to the database.

    Args:
        users_to_add (str): A string of comma-separated usernames of the lobby members.
        game (str): The game associated with the lobby.
        state (str): The state of the lobby.

    Returns:
        int: The ID of the newly added lobby.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute(
        'INSERT INTO lobbies (users, game, scores, state) VALUES (?, ?, "None", ?)',
        (users_to_add, game, state)
    )
    conn.commit()
    cursor.close()
    conn.close()

    print(f'Lobby inserted {users_to_add}')

    return fetch_lobby_id_by_users(users_to_add)


def fetch_lobby_id_by_users(users_to_fetch: str) -> int:
    """
    Fetches the lobby ID by users.

    Args:
        users_to_fetch (str): A string of comma-separated usernames of the lobby members.

    Returns:
        int: The ID of the lobby.
    """
    print(f'Fetching: {users_to_fetch}')

    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM lobbies WHERE users = ?', (users_to_fetch,))
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    if rows:
        for row in rows:
            if row[4] != 'game ended':
                return row[0]  # Return lobby ID

    return -1


def fetch_lobby_by_id(id: int) -> dict[str, any]:
    """
    Fetches a lobby by its ID.

    Args:
        id (int): The ID of the lobby.

    Returns:
        dict[str, any]: A dictionary containing lobby information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM lobbies WHERE id = ?', (id,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        return {"id": row[0], "users": row[1], "game": row[2],
                "scores": row[3], "state": row[4]}  # Return lobby scores

    print(f'No lobby found! {id}')
    return {"lobby": "not found!"}


def fetch_lobby_by_id_raw(id: int) -> list[str]:
    """
    Fetches a lobby by its ID in raw format.

    Args:
        id (int): The ID of the lobby.

    Returns:
        list[str]: A list containing lobby information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM lobbies WHERE id = ?', (id,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        return [row[0], row[1], row[2], row[3], row[4]]  # Return lobby scores

    print(f'No lobby found! {id}')
    return ["lobby not found!"]


def update_lobby_scores(id: int, scores: str) -> None:
    """
    Updates the scores of a lobby.

    Args:
        id (int): The ID of the lobby.
        scores (str): The new scores for the lobby.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()

    cursor.execute(
        'UPDATE lobbies SET scores = ? WHERE id = ?',
        (scores, id,)
    )
    print(f"Added scores: {scores}")

    conn.commit()
    cursor.close()
    conn.close()


def update_lobby_state(id: int, state: str) -> None:
    """
    Updates the state of a lobby.

    Args:
        id (int): The ID of the lobby.
        state (str): The new state for the lobby.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()

    cursor.execute(
        'UPDATE lobbies SET state = ? WHERE id = ?',
        (state, id,)
    )
    print(f"Added: {state}")

    conn.commit()
    cursor.close()
    conn.close()


def append_lobby_state(lobby_id: int, append_state: str) -> None:
    """
    Appends a state to an existing lobby state.

    Args:
        lobby_id (int): The ID of the lobby.
        append_state (str): The state to append.
    """
    lobby = fetch_lobby_by_id(lobby_id)
    update_lobby_state(lobby_id, lobby["state"] + append_state)


def fetch_user_by_name(username: str) -> dict[str, any] | None:
    """
    Fetches a user by their username.

    Args:
        username (str): The username of the user.

    Returns:
        dict[str, any] | None: A dictionary containing user information or None if the user is not found.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE username = ?', (username,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        return {'username': row[1], 'email': row[2], 'birthday': row[3],
                'password': row[4], 'online': row[5], 'id': row[0]}
    else:
        return None


def fetch_data_by_id(id: int) -> dict[str, any] | None:
    """
    Fetches a user by their ID.

    Args:
        id (int): The ID of the user.

    Returns:
        dict[str, any] | None: A dictionary containing user information or None if the user is not found.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE id = ?', (id,))
    row = cursor.fetchone()
    cursor.close()
    conn.close()

    if row:
        return {'username': row[1], 'email': row[2], 'birthday': row[3],
                'password': row[4], 'online': row[5], 'id': row[0]}
    else:
        return None


def get_user_in_lobby(lobby_id: int) -> list[str]:
    """
    Retrieves the usernames of users in a lobby.

    Args:
        lobby_id (int): The ID of the lobby.

    Returns:
        list[str]: A list of usernames in the lobby.
    """
    lobby = fetch_lobby_by_id(lobby_id)
    usernames_str = lobby['users']
    return usernames_str.split(', ')


def remove_party(leader: str) -> None:
    """
    Removes a party led by the specified leader.

    Args:
        leader (str): The username of the party leader.
    """
    threading.Thread(target=remove_party_thread, args=[leader]).start()
    print('Deletion thread started')


def remove_party_thread(leader: str) -> None:
    """
    Threaded function to remove a party led by the specified leader.

    Args:
        leader (str): The username of the party leader.
    """
    members = get_members_in_party(leader)
    flag = True

    while flag:
        flag2 = True

        for username in members:
            user = fetch_user_by_name(username)
            if user['online'] == 1:
                flag2 = False

        if flag2:
            flag = False

    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute("DELETE FROM parties WHERE leader = ?", (leader,))
    conn.commit()
    cursor.close()
    conn.close()

    print(f'Party led by: {leader} deleted.')


def fetch_all_lobbies() -> list[dict]:
    """
    Fetches all lobbies from the database.

    Returns:
        list[dict]: A list of dictionaries containing lobby information.
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM lobbies")
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows


def get_members_in_party(leader: str) -> list:
    """
    Retrieves the members of a party led by the specified leader.

    Args:
        leader (str): The username of the party leader.

    Returns:
        list: A list of usernames in the party.
    """
    exists, users_in_party = search_party_by_leader(leader)
    print(f'Users in party: {users_in_party}')

    if not exists:
        return []  # Single player

    return users_in_party.split(', ')
