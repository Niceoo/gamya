from fastapi import APIRouter, Query, Depends, HTTPException
from pydantic import BaseModel
from random import randint
from API.global_funcs import (verify_token, add_lobby, remove_online, fetch_lobby_id_by_users, get_members_in_party,
                              get_update_json, client_req, fetch_lobby_by_id, update_lobby_state, fetch_lobby_state,
                              remove_party)

router = APIRouter()


class PostTicTacToe(BaseModel):
    username: str
    leader: str


class PlaceModel(BaseModel):
    lobby_id: int
    index: int
    user_type: str


def get_type(value: int, opposite: bool = False) -> str:
    """
    Get the type of the user for Tic Tac Toe (X or O).

    Args:
        value (int): The value to determine the type.
        opposite (bool, optional): Whether to get the opposite type. Defaults to False.

    Returns:
        str: The type of the user ('X' or 'O').
    """
    if opposite:
        value = value ^ 1

    if value == 1:
        return 'X'
    else:
        return 'O'


def generate_state_string(state: list) -> str:
    """
    Generate a string representation of the Tic Tac Toe board state.

    Args:
        state (list): The list representing the state of the Tic Tac Toe board.

    Returns:
        str: The string representation of the board state.
    """

    state_str = ""

    for pos in state:
        if pos not in ['X', 'O']:
            state_str += ','
        else:
            state_str += f'{pos},'

    return state_str


def has_won(state: list) -> str:
    """
    Determine if a player has won the Tic Tac Toe game.

    Args:
        state (list): The list representing the state of the Tic Tac Toe board.

    Returns:
        str: The symbol of the player who has won ('X' or 'O') or 'N' if the game is a draw.
    """

    for i in range(3):
        if state[i*3] != '':
            if state[i*3] == state[i*3+1] == state[i*3+2]:
                return state[i*3]
        if state[i] != '':
            if state[i] == state[i+3] == state[i+6]:
                return state[i]

    if state[0] != '' and state[0] == state[4] == state[8]:
        return state[0]

    if state[2] != '' and state[2] == state[4] == state[6]:
        return state[2]

    for index in range(9):
        if state[index] == "":
            return ""

    return 'N'  # the board has filled, winner is None


@router.post('/tictactoe', dependencies=[Depends(verify_token)])
def create_tictactoe_lobby(post: PostTicTacToe):
    """
    Function to create a Tic Tac Toe lobby.

    Args:
        post (PostTicTacToe): The request body containing the username and leader.

    Returns:
        dict: A dictionary containing the lobby_id and user_type.
    """

    if post.leader != 'singlePlayer':
        if post.leader == post.username:
            # the requester is the leader of the party
            requester = 'leader'

            member_list: list = get_members_in_party(post.leader)

            if len(member_list) > 1:
                raise HTTPException(status_code=400, detail="Must be a party of 2")

            fresh_state: list[str] = ['' for _ in range(9)]
            rand: int = randint(0, 1)

            string_state: str = ",".join(fresh_state)

            string_state = string_state + f',#{rand}'

            print(f'creating lobby for {post.leader}, {post.username}')

            lobby_id: int = add_lobby(', '.join([post.leader] + member_list), 'Tic Tac Toe', state=string_state)
            print(f"the new id is: {lobby_id}")

            for member in member_list:
                data: dict[str, any] = get_update_json(
                    {
                        "msg": f"your party leader started a TicTacToe game",
                        "lobby_id": lobby_id,
                        "game": "TicTacToe",
                        "type": "enter_game"
                    }
                )
                client_req[member] = data

            remove_party(post.leader)

        else:
            print("request from member")
            requester = 'member'
    else:
        # user is in single player mode.
        raise HTTPException(status_code=400, detail="Must be a party of 2")

    remove_online(post.username)

    lobby_id: int = fetch_lobby_id_by_users(', '.join([post.leader] + get_members_in_party(post.leader)))

    state: str = fetch_lobby_state(lobby_id)
    if requester == 'leader':
        user_type: str = get_type(int(state.split('#')[1]))
    else:
        user_type: str = get_type(int(state.split('#')[1]), True)

    return {
        'lobby_id': lobby_id,
        'user_type': user_type
    }


@router.get('/tictactoe/board', dependencies=[Depends(verify_token)])
def get_board(lobby_id: int = Query(...)):
    """
    Get the Tic Tac Toe board state.

    Args:
        lobby_id (int): The ID of the Tic Tac Toe lobby.

    Returns:
        dict: A dictionary containing the Tic Tac Toe board state.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    state: str = lobby['state']

    return_json = {}

    if state[-1] in ['X', 'O', 'N']:
        return_json['won'] = state[-1]

    return_json["board"] = state.split('#')[0][:-1].split(',')

    return return_json


@router.post('/tictactoe/place', dependencies=[Depends(verify_token)])
def place_at_index(post: PlaceModel):
    """
    Place a symbol on the Tic Tac Toe board.

    Args:
        post (PlaceModel): The request body containing the lobby_id, index, and user_type.

    Returns:
        None
    """

    user_type = post.user_type

    lobby: dict[str, any] = fetch_lobby_by_id(post.lobby_id)

    state: list[str] = lobby['state'].split(',')

    state[post.index] = user_type

    state_string: str = generate_state_string(state[:-1]) + state[-1]

    if won := has_won(state):
        state_string += won

    update_lobby_state(post.lobby_id, state_string)


