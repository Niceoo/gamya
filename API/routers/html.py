from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

import sys
sys.path.append("..")  # Adds higher directory to python modules path.

templates = Jinja2Templates(directory="templates")

try:
    from global_funcs import fetch_all_data_tuple
except ImportError as e:
    print(f"ImportError: {e}")
    sys.exit(1)

router = APIRouter()


@router.get('/', response_class=HTMLResponse)
def index(request: Request):
    """
    Renders the index page with data fetched from the database.

    Args:
    - request (Request): The request object.

    Returns:
    - TemplateResponse: HTML page with data from the database.
    """
    # Fetch data from the database
    (users, parties, lobbies) = fetch_all_data_tuple()

    return templates.TemplateResponse('index.html',
                                      {'request': request, 'users': users, 'parties': parties, 'lobbies': lobbies})


@router.get('/refresh', response_class=HTMLResponse)
def index_refresh(request: Request):
    """
    Refreshes and renders the index page with updated data fetched from the database.

    Args:
    - request (Request): The request object.

    Returns:
    - TemplateResponse: HTML page with updated data from the database.
    """
    # Fetch data from the database
    (users, parties, lobbies) = fetch_all_data_tuple()

    return templates.TemplateResponse('index.html',
                                      {'request': request, 'users': users, 'parties': parties, 'lobbies': lobbies})
