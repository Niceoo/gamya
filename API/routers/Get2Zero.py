from fastapi import APIRouter, Query, Depends, HTTPException
from pydantic import BaseModel

from API.global_funcs import verify_token, add_lobby, remove_online, fetch_lobby_id_by_users, get_members_in_party

router = APIRouter()


class PostZero(BaseModel):
    username: str


def find_optimal_queue(number: float, percentage: float, dec: int) -> str:
    """
    Finds the optimal queue given a number, a percentage, and the initial subtraction.

    Args:
    - number (float): The initial number.
    - percentage (float): The percentage to subtract.
    - dec (int): The initial subtraction value.

    Returns:
    - str: The optimal number of clicks to reach zero.
    """
    level: int = 0
    queue: list[list] = [[number, dec, level]]
    flag: bool = False
    optimal_clicks: any = float('inf')

    while len(queue) > 0:
        num, temp_dec, lvl = queue.pop(0)

        if num <= 0:
            optimal_clicks = min(optimal_clicks, level)
            flag = True
        if temp_dec >= number:
            optimal_clicks = min(optimal_clicks, level + 1)
            flag = True

        queue.append([num, temp_dec + 1, lvl + 1])
        queue.append([num - temp_dec, temp_dec, lvl + 1])
        queue.append([(1 - percentage / 100) * num, temp_dec, lvl+1])

        #  gone through each append of the current level
        if queue[0][2] == level + 1:
            if flag:
                break
            level += 1

            num_to_dec = {}
            dec_to_num = {}

            for option in queue:
                num, temp_dec, lvl = option

                current_num = dec_to_num.get(temp_dec)

                if not current_num:
                    dec_to_num[temp_dec] = num
                else:
                    if num <= current_num:
                        dec_to_num[temp_dec] = num

            queue = [[num, temp_dec, level] for num, temp_dec in dec_to_num.items()]

            for option in queue:
                num, temp_dec, lvl = option

                current_dec = num_to_dec.get(num)

                if not current_dec:
                    num_to_dec[num] = temp_dec
                else:
                    if temp_dec >= current_dec:
                        num_to_dec[num] = temp_dec
                        last_best_num = dec_to_num.get(temp_dec)

                        if not last_best_num:
                            continue
                        if last_best_num >= num:
                            del dec_to_num[temp_dec]

            queue = [[num, temp_dec, level] for temp_dec, num in num_to_dec.items()]

    return str(optimal_clicks)


@router.post('/zero', dependencies=[Depends(verify_token)])
def create_zero_lobby(post: PostZero):
    """
    Creates a lobby for the Zero game.

    Args:
    - post (PostZero): The POST request body containing the username.

    Returns:
    - dict: A dictionary containing the lobby ID.
    """

    member_list: list = get_members_in_party(post.username)
    print(member_list)

    if len(member_list) > 0:
        raise HTTPException(status_code=400, detail="Must be single player")

    add_lobby(post.username, 'Get 2 Zero')

    remove_online(post.username)

    return_json = {'lobby_id': fetch_lobby_id_by_users(post.username)}

    print(return_json)

    return return_json


@router.get('/zero/optimal', dependencies=[Depends(verify_token)])
def get_optimal(number: float = Query(...),
                percent: float = Query(...),
                dec: int = Query(...)
                ):
    """
    Get the optimal number of clicks to reach zero in a Zero game.

    Args:
    - number (float): The initial number.
    - percent (float): The percentage to subtract.
    - dec (int): The number of decimal places to round to.

    Returns:
    - dict: A dictionary containing the optimal number of clicks.
    """
    print(find_optimal_queue(number, percent, dec))
    return {"optimal": find_optimal_queue(number, percent, dec)}
