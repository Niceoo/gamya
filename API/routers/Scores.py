from fastapi import Query, Depends, APIRouter
from pydantic import BaseModel

from API.global_funcs import (fetch_lobby_by_id, verify_token, update_lobby_scores,
                              count_users_in_lobby, update_lobby_state)

has_lobby_ended = {}

router = APIRouter()


class ScorePost(BaseModel):
    username: str
    score: float
    lobby_id: int


@router.get('/getScore')
def get_score(lobby_id: int = Query(...)):
    """
    Get the scores of users in a lobby.

    Args:
        lobby_id (int): The ID of the lobby.

    Returns:
        Union[list[dict], dict]: List of dictionaries containing usernames and scores, or a dictionary
        indicating no results yet.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    scores: list[str] = lobby["scores"].split(', ')

    if scores == ['None']:
        return {"username": 'no results yet', "scores": ''}

    json_list: list[dict] = []

    for score in scores:
        username, cur_score = score.split(': ')
        json_list.append({"username": username, "score": cur_score})

    return json_list


@router.post('/postScore', dependencies=[Depends(verify_token)])
def post_score(post: ScorePost):
    """
    Post scores to the lobby.

    Args:
        post (ScorePost): The score post request containing username, score, and lobby_id.

    Returns:
        dict: A dictionary indicating the status of the score posting.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(post.lobby_id)

    print(lobby, post.lobby_id)

    if lobby['scores'] == "None":
        lobby['scores'] = f"{post.username}: {post.score}"
    else:
        json_list: list = []

        for score in lobby['scores'].split(', '):
            username, cur_score = score.split(': ')
            json_list.append([username, cur_score])

        json_list.append([post.username, post.score])

        sorted_list: list = sorted(json_list, key=lambda x: float(x[1]), reverse=True)

        scores = ", ".join([f'{username}: {score}' for username, score in sorted_list])

        print(scores)

        lobby['scores'] = scores

    if has_lobby_ended.get(post.lobby_id) is None:
        has_lobby_ended[post.lobby_id] = 1
    else:
        has_lobby_ended[post.lobby_id] += 1

    if has_lobby_ended.get(post.lobby_id) == count_users_in_lobby(post.lobby_id):
        update_lobby_state(post.lobby_id, 'game ended')

    update_lobby_scores(lobby['id'], lobby['scores'])
