import json

from fastapi import HTTPException, APIRouter, Query, Depends
from pydantic import BaseModel
from hashlib import md5

from API.global_funcs import (client_req, fetch_all_usernames, fetch_all_online, re_add_online,
                              scan_user, generate_access_token, insert_user, delete_user, get_new_user_id, verify_token)

PEPPER: bytes = b'|||'
router = APIRouter()


# Pydantic model for user data
class User(BaseModel):
    username: str
    email: str
    birthday: str
    password: str
    online: int


# Function to validate signup data
def is_valid_signup(username: str, email: str, password: str) -> str:
    """
    Validate the signup data.

    Args:
        username (str): The username.
        email (str): The email address.
        password (str): The password.

    Returns:
        str: The status of the signup data.
    """
    exists, id, user_password = scan_user(username)

    if exists:
        if md5(password.encode('utf-8') + PEPPER).hexdigest() == user_password:
            return f'existing user#{id}'
        else:
            return f'password missmatch#{id}'
    elif (not exists) and (email.split('@')[0] == 'signInEmail'):
        return 'user not registered'

    return 'valid'


@router.get('/users')
def get_online_users(username: str):
    """
    Return a JSON of the online users besides the requester.

    Args:
        username (str): The username of the requester.

    Returns:
        dict: A dictionary containing the online users.
    """
    online: list[str] = fetch_all_online()

    if username in online:
        online.remove(username)

    return {'users': online}


@router.post('/user')
def add_user(user: User):
    """
    Add a new user.

    Args:
        user (User): The user data.

    Returns:
        dict: A dictionary containing the access token, token type,
         expiration time, user ID, and whether the user is new.
    """
    status: str = is_valid_signup(user.username, user.email, user.password)

    is_new: bool = True
    clients_access_key: str | None = None
    client_id: int = 0

    if status == 'user not registered':
        raise HTTPException(status_code=422, detail="New user")
    elif status.split('#')[0] == 'password missmatch':
        raise HTTPException(status_code=422, detail="Passwords don't match!")
    elif status.split('#')[0] == 'existing user':
        is_new = False
        client_id = int(status.split('#')[1])
        clients_access_key = generate_access_token(client_id, user.username)
        client_req.update({user.username: {}})
        re_add_online(user.username)

    if is_new:
        client_id = get_new_user_id()
        encoded_password = md5(user.password.encode('utf-8') + PEPPER).hexdigest()

        print(user)

        insert_user(client_id, user.username, user.email, user.birthday, encoded_password)
        clients_access_key = generate_access_token(client_id, user.username)

        client_req.update({user.username: {}})

    return {
        'access_token': clients_access_key,
        'token_type': 'Bearer',
        'expires_in': 3600,
        'id': client_id,
        'isNew': is_new
    }


@router.get("/user/{username}", dependencies=[Depends(verify_token)])
def get_user_data(username: str,
                  get: str = Query(...),
                  change: str = Query(...)):
    """
    Get or update user data.

    Args:
        username (str): The username.
        get (str): Whether to retrieve data. Defaults to 'true'.
        change (str): Whether to update data. Defaults to 'true'.

    Returns:
        dict: A dictionary containing the user data.
    """
    if username not in fetch_all_usernames():
        raise HTTPException(status_code=404, detail="User not found!")

    if get == 'true':
        # Retrieve data associated with the username
        data = client_req.get(username)

        if data is None:
            return {"isUpdate": 'true',
                    'data': {'type': 'error'}
                    }

        return data

    else:
        change_dict = json.loads(change)
        # Update the client requests
        client_req[username] = change_dict


@router.delete('/user/{id}', dependencies=[Depends(verify_token)])
def delete_user_route(id: int) -> dict[str, str]:
    """
    Delete a user.

    Args:
        id (int): The ID of the user.

    Returns:
        dict: A dictionary containing a message.
    """
    delete_user(id)
    return {'message': 'User deleted'}
