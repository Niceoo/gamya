import random

from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel

from API.global_funcs import (client_req, get_update_json, verify_token, fetch_lobby_id_by_users, get_members_in_party,
                              add_lobby, remove_online, fetch_lobby_by_id, remove_party)

router = APIRouter()


class PostMathTest(BaseModel):
    username: str
    leader: str


class PostAnswer(BaseModel):
    lobby_id: int
    index: int
    answer: str


def generate_math_problem() -> dict:
    """
    Generate a math problem with random operands and operations.

    Returns:
        dict: Dictionary containing the equation and its options.
    """
    op1 = random.randint(1, 100)
    op2 = random.randint(1, 10)
    op3 = random.randint(1, 10)
    op4 = random.randint(1, 10)

    # Randomly select operations
    operation1 = random.choice(['+', '-', '*', '/'])
    operation2 = random.choice(['+', '-', '*', '/'])
    operation3 = random.choice(['+', '-', '*', '/'])

    # Create the equation
    equation = f"{op1} {operation1} {op2} {operation2} {op3} {operation3} {op4}"

    # although eval is vulnerable, the inserted function is pretty much hard coded, so it doesn't pose a threat.
    correct_answer = eval(equation)

    rounded_correct_answer = round(correct_answer, 2)
    # Generate three incorrect options
    options = [rounded_correct_answer]
    while len(options) < 4:
        incorrect_option = round(rounded_correct_answer + random.randint(-10, 10), 2)  # Generating nearby numbers
        if incorrect_option != rounded_correct_answer and incorrect_option not in options:
            options.append(incorrect_option)

    # Shuffle the options
    random.shuffle(options)

    # Create the problem object
    problem = {
        "equation": equation,
        "op1": options[0],
        "op2": options[1],
        "op3": options[2],
        "op4": options[3],
        "correct": rounded_correct_answer
    }

    return problem


def get_equations_str(amount: int) -> str:
    """
    Generate a string representation of multiple math problems.

    Args:
        amount (int): Number of math problems to generate.

    Returns:
        str: String representation of the math problems.
    """
    equations: list[dict] = [generate_math_problem() for _ in range(amount)]

    eq_string: str = '#'.join([f'{equation["equation"]},{equation["op1"]},{equation["op2"]},{equation["op3"]},'
                               f'{equation["op4"]},{equation["correct"]}' for equation in equations])

    return eq_string


def get_equations_dict(state: str) -> list[dict]:
    """
    Convert string representation of equations to a list of dictionaries.

    Args:
        state (str): String representation of equations.

    Returns:
        list[dict]: List of dictionaries containing equations and options.
    """
    equations_list: list[str] | list[list[str]] = state.split('#')

    equations_list = [eq.split(',') for eq in equations_list]

    dict_questions: list[dict] = [{"equation": eq[0], "op1": eq[1], "op2": eq[2], "op3": eq[3], "op4": eq[4],
                                   "correct": eq[5]} for eq in equations_list]

    return dict_questions


def get_lobby_math_data(lobby_id: int, index: int) -> dict[str, str]:
    """
    Get math problem data from the lobby.

    Args:
        lobby_id (int): ID of the lobby.
        index (int): Index of the math problem.

    Returns:
        dict[str, str]: Math problem data.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    state: str = lobby["state"]

    equations: list[dict] = get_equations_dict(state)

    return equations[index]


@router.post('/mathTest', dependencies=[Depends(verify_token)])
def create_math_lobby(post: PostMathTest):
    """
    Function to create a math test lobby.

    Args:
        post (PostMathTest): Post request containing username and leader information.

    Returns:
        dict: Dictionary containing the lobby_id.
    """

    if post.leader != 'singlePlayer':
        if post.leader == post.username:
            # the requester is the leader of the party

            member_list = get_members_in_party(post.leader)

            equations: str = get_equations_str(10)

            lobby_id = add_lobby(', '.join([post.leader] + member_list), 'Math Test', equations)
            print(f"the new id is: {lobby_id}")

            for member in member_list:
                data: dict[str, any] = get_update_json(
                    {
                        "msg": f"your party leader started a math test game",
                        "lobby_id": lobby_id,
                        "game": "Math test",
                        "type": "enter_game"
                    }
                )

                client_req[member] = data
            remove_party(post.leader)
        else:
            print("request from member")
    else:
        equations: str = get_equations_str(10)

        add_lobby(post.username, 'Math Test', equations)

        remove_online(post.username)

        lobby_id = fetch_lobby_id_by_users(post.username)

        print(lobby_id)

        return {
            'lobby_id': lobby_id
        }

    remove_online(post.username)

    lobby_id = fetch_lobby_id_by_users(', '.join([post.leader] + get_members_in_party(post.leader)))

    print(lobby_id)

    return {
        'lobby_id': lobby_id
    }


@router.get('/getMath', dependencies=[Depends(verify_token)])
def get_math_data(lobby_id: int = Query(...)):
    """
    Function to get math test data.

    Args:
        lobby_id (int): ID of the lobby.

    Returns:
        dict: Dictionary containing the length of equations and equations data.
    """

    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    state: str = lobby["state"]

    # print(state)

    equations: list[str] | list[list[str]] = state.split("#")

    equations = [eq.split(',') for eq in equations]

    # print(equations)

    dict_eq: list[dict] = [{"equation": eq[0], "op1": eq[1], "op2": eq[2], "op3": eq[3],"op4": eq[4], "correct": eq[5]}
                           for eq in equations]

    # print(dict_eq)

    return {
        'length': len(dict_eq),
        'equations': dict_eq
    }


@router.post('/math/answer', dependencies=[Depends(verify_token)])
def post_math_answer(post: PostAnswer):
    """
    Function to check the correctness of the math answer.

    Args:
        post (PostAnswer): Post request containing lobby_id, index, and answer.

    Returns:
        dict: Dictionary indicating whether the answer is correct or not.
    """

    dict_ques: dict[str, str] = get_lobby_math_data(post.lobby_id, post.index)

    print(dict_ques)

    return {
        'correct': dict_ques['correct'] == post.answer
    }
