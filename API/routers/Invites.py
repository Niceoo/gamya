from fastapi import Depends, HTTPException, Response, APIRouter
from pydantic import BaseModel

from API.global_funcs import verify_token, client_req, fetch_all_usernames, add_party_member


router = APIRouter()


class UserInvite(BaseModel):
    sender_username: str
    receiver_username: str


@router.post('/inviteUser', dependencies=[Depends(verify_token)])
def invite_user(user_invite: UserInvite):
    """
    Endpoint to invite a user to join a party.

    Args:
        user_invite (UserInvite): The invitation details.

    Returns:
        dict: Message indicating that the user has been invited.
    """
    new_data: dict[str, str] = \
        {"isUpdate": 'true',
         "data": {
             "info": {
                 "msg": f"Invited by {user_invite.sender_username}.",
                 "name": user_invite.sender_username
             },
             "type": "msg"}
         }
    client_req[user_invite.receiver_username] = new_data
    return {"msg": f"{user_invite.receiver_username} has been Invited!"}


@router.post('/declineInvite', dependencies=[Depends(verify_token)])
def decline_user_invite(user_invite: UserInvite):
    """
    Endpoint to decline a user invitation.

    Args:
        user_invite (UserInvite): The invitation details.

    Returns:
        dict: Message indicating that the user has been notified.
    """
    new_data: dict[str, str] = \
        {"isUpdate": 'true',
         "data": {
             "info": {
                 "msg": f"{user_invite.sender_username} has declined your invite.", "name": user_invite.sender_username
             },
             "type": "decline"}
         }

    client_req[user_invite.receiver_username] = new_data
    return {"msg": f"{user_invite.receiver_username} has been notified."}


@router.post('/acceptInvite', dependencies=[Depends(verify_token)])
def accept_user_invite(user_invite: UserInvite):
    """
    Endpoint to accept a user invitation.

    Args:
        user_invite (UserInvite): The invitation details.

    Returns:
        dict: Message indicating that the user has been notified.
    """
    if user_invite.receiver_username not in fetch_all_usernames():
        raise HTTPException(status_code=404, detail="User not found!")

    add_party_member(user_invite.receiver_username, user_invite.sender_username)

    new_data: dict[str, str] = \
        {"isUpdate": 'true',
         "data": {
             "msg": f"{user_invite.sender_username} has accepted your invite.",
             "name": user_invite.sender_username,
             "type": "accept"}
         }

    client_req[user_invite.receiver_username] = new_data
    return {"msg": f"{user_invite.receiver_username} has been notified."}
