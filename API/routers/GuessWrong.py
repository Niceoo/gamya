import json
import random
from collections import deque
from time import sleep

from fastapi import Depends, Query, HTTPException, APIRouter
from pydantic import BaseModel

from API.global_funcs import (verify_token, get_members_in_party, add_lobby, get_update_json, client_req, remove_online,
                              fetch_lobby_id_by_users, fetch_lobby_by_id, update_lobby_state, append_lobby_state,
                              update_lobby_scores, get_user_in_lobby, remove_party)

router = APIRouter()
user_info = {}
initial_usernames = []


class PostGuessWrong(BaseModel):
    username: str
    leader: str


class PostCategory(BaseModel):
    lobby_id: int
    username: str
    category: str
    secret_word: str


class PostGuess(BaseModel):
    lobby_id: int
    username: str
    guess: str


def check_guess_over(lobby_id: int) -> None:
    """
    Checks if the guessing game is over.

    Args:
    - lobby_id (int): The ID of the lobby.
    """
    # state looks like this: "user-queue#current,category,word/guesser,guess|guesser,guess|guesser,guess...
    state: str = fetch_lobby_by_id(lobby_id)["state"]

    creator_name, _, word = state.split('#')[1].split('/')[0].split(',')

    guesses: list = state.split('/')[1].split('|')[:-1]

    # print(len(guesses))
    # print(guesses)
    # print(len(fetch_lobby_by_id(lobby_id).get("users").split(', ')))

    if len(guesses) + 1 >= len(get_user_in_lobby(lobby_id)):
        # all have posted guess
        score_string = ''
        for guess in guesses:
            guesser, temp_guess = guess.split(',')

            if temp_guess != word:
                score_string += f'{guesser},100|'
            else:
                score_string += f'{guesser},0|'

        add_scores(lobby_id, score_string)

        for username in get_user_in_lobby(lobby_id):
            user_info[username] = {"data": {"getScore": 'true'},
                                   "isUpdate": 'true'}

        while not await_clients_recv():
            # print('waiting')
            pass

        print('clients got passes')

        queue: deque[str] = deque(state.split('#')[0].split(','))

        queue.rotate(1)

        if list(queue)[0] in initial_usernames:
            notify_game_end(lobby_id)
            print('notified game end')
            return

        update_lobby_state(lobby_id, ','.join(queue) + '#')
        notify_current(list(queue))

        await_clients_recv()


def await_clients_recv():
    """
    Checks if clients have received updates.

    Returns:
    - bool: True if all clients have received updates, False otherwise.
    """
    for user, status in user_info.items():
        if status.get('isUpdate') == 'true':
            return False
    return True


def notify_current(users: list) -> None:
    """
    Notifies users about the current state.

    Args:
    - users (list): List of usernames.
    """

    for index, user in enumerate(users):
        if index == 0:
            print(f'current: {user}')
            user_info[user] = {"data": {"current": 'true'},
                               "isUpdate": 'true'}
            continue
        else:
            user_info[user] = {"data": {"current": 'false'},
                               "isUpdate": 'true'}


def notify_category(users: list, category: str) -> None:
    """
    Notifies users about the chosen category.

    Args:
    - users (list): List of usernames.
    - category (str): The chosen category.
    """
    for index, user in enumerate(users):
        if index == 0:
            continue
        print(f'user: {user} notified about: {category}')
        user_info[user] = {"data": {"category": category},
                           "isUpdate": 'true'}


def notify_game_end(lobby_id: int) -> None:
    """
    Notifies users about the end of the game.

    Args:
    - lobby_id (int): The ID of the lobby.
    """
    for username in get_user_in_lobby(lobby_id):
        user_info[username] = {"data": {"gameOver": 'true'},
                               "isUpdate": 'true'}

    update_lobby_state(lobby_id, "game ended")


def add_scores(lobby_id: int, score_str: str) -> None:
    """
    Updates user scores in the lobby.

    Args:
    - lobby_id (int): The ID of the lobby.
    - score_str (str): String containing user scores.
    """
    scores: str = fetch_lobby_by_id(lobby_id)["scores"]

    updated_score: list[str] = []

    if scores == "None":
        for score in score_str.split('|')[:-1]:
            username, update_score = score.split(',')
            updated_score.append(f'{username}: {update_score}')

        update_lobby_scores(lobby_id, ', '.join(updated_score))
        return

    for user_score in score_str.split('|')[:-1]:
        user, update_score = user_score.split(',')
        flag = False

        for score in scores.split(', '):
            username, cur_score = score.split(': ')

            if user == username:
                flag = True

                updated_score.append(f'{username}: {int(cur_score) + int(update_score)}')
                break

        if not flag:
            updated_score.append(f'{user}: {update_score}')

    currently_existing_scores: list = []
    for score in updated_score:
        username, s = score.split(': ')
        currently_existing_scores.append(username)

    for score in scores.split(', '):
        username, cur_score = score.split(': ')

        if username not in currently_existing_scores:
            print(f'new user {username}')
            updated_score.append(f'{username}: {cur_score}')

    print(updated_score)

    update_lobby_scores(lobby_id, ', '.join(updated_score))


def update_user_info(users: list[str]):
    """
    Updates the user information dictionary.

    Args:
    - users (list): List of usernames.
    """
    for user in users:
        user_info.update({user: {"isUpdate": 'false'}})


@router.post('/guessWrong', dependencies=[Depends(verify_token)])
def create_guess_lobby(post: PostGuessWrong):
    """
    Creates a lobby for the Guess Wrong game.

    Args:
    - post (PostGuessWrong): The POST request body containing the username and leader.

    Returns:
    - dict: A dictionary containing the lobby ID.
    """

    if post.leader != 'singlePlayer':
        if post.leader == post.username:
            # the requester is the leader of the party

            member_list: list = get_members_in_party(post.leader)

            users: list = [post.leader] + member_list

            update_user_info(users)

            lobby_id = add_lobby(', '.join(users), 'Guess wrong')

            random.shuffle(users)

            initial_usernames.append(users[0])

            state_string: str = ','.join(users) + "#"

            update_lobby_state(lobby_id, state_string)

            for member in member_list:
                data: dict[str, any] = get_update_json(
                    {
                        "msg": f"your party leader started a GuessWrong game",
                        "lobby_id": lobby_id,
                        "game": "Guess Wrong",
                        "type": "enter_game"
                    }
                )

                client_req[member] = data

            notify_current(users)

            remove_party(post.leader)

        else:
            print("request from member")
    else:

        raise HTTPException(status_code=422, detail="Unavailable for 1 player")

    remove_online(post.username)

    lobby_id = fetch_lobby_id_by_users(', '.join([post.leader] + get_members_in_party(post.leader)))

    return {
        'lobby_id': lobby_id
    }


@router.get('/guessWrong/{username}', dependencies=[Depends(verify_token)])
def get_guess_score(username: str,
                    get: str = Query(...),
                    change: str = Query(...)):
    """
    Retrieves or updates the guess scores for a user.

    Args:
    - username (str): The username to retrieve or update scores for.
    - get (str): Flag indicating whether to retrieve data (true) or not (false).
    - change (str): JSON string containing changes to be made.

    Returns:
    - dict: The user data or an error message.
    - None: if the client request a change
    """
    if get == 'true':
        # Retrieve data associated with the username
        data = user_info.get(username)

        if data is None:
            print(f"client no longer in guessWrong {username}")

            return {"isUpdate": 'true',
                    "data": {
                        "info": {
                            "msg": f"client no longer online."
                        },
                        "type": "error"}
                    }

        # Render the page for the user using the retrieved data
        return data
    else:
        change_dict = json.loads(change)
        # Update the client requests
        user_info[username] = change_dict


@router.post('/guessWrong/category', dependencies=[Depends(verify_token)])
def post_category(post: PostCategory):
    """
    Sets the category for the Guess Wrong game.

    Args:
    - post (PostCategory): The POST request body containing lobby ID, username, category, and secret word.
    """

    append_lobby_state(post.lobby_id, f'{post.username},{post.category},{post.secret_word}/')

    state: str = fetch_lobby_by_id(post.lobby_id)["state"]

    notify_category(state.split('#')[0].split(','), post.category)


@router.post('/guessWrong/guess', dependencies=[Depends(verify_token)])
def post_guess(post: PostGuess):
    """
    Processes a user's guess in the Guess Wrong game.

    Args:
    - post (PostGuess): The POST request body containing lobby ID, username, and guess.
    """
    append_lobby_state(post.lobby_id, f'{post.username},{post.guess}|')

    check_guess_over(post.lobby_id)
