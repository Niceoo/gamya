import json
from fastapi import Depends, Query, HTTPException, APIRouter
from pydantic import BaseModel

from API.global_funcs import (verify_token, get_members_in_party, add_lobby, get_update_json, client_req, remove_online,
                              fetch_lobby_id_by_users, remove_party)

router = APIRouter()


class PostVocab(BaseModel):
    username: str
    leader: str


class PostAnswer(BaseModel):
    diff: str
    index: int
    answer: str


def get_vocab_data(diff: str, index: int = -1) -> list[dict] | dict[str, str]:
    """
    Get vocabulary data.

    Args:
        diff (str): The difficulty level.
        index (int, optional): The index of the vocabulary data. Defaults to -1.

    Raises:
        HTTPException: If the difficulty level is unmatched.

    Returns:
        list[dict] | dict[str, str]: The vocabulary data.
    """

    with open('vocab_list.json', 'r') as file:
        vocab_json_diffs: dict = json.load(file)

        if diff in vocab_json_diffs.keys():
            vocab_json: list[dict] = vocab_json_diffs[diff]
            print(vocab_json)
        else:
            print("unmatched diff")
            raise HTTPException(status_code=400, detail="Unmatched difficulty!")

    if index != -1:
        return vocab_json[index]

    return vocab_json


def create_state_string(vocab_json: list[dict]) -> str:
    """
    Create a state string from vocabulary data.

    Args:
        vocab_json (list[dict]): The vocabulary data.

    Returns:
        str: The state string.
    """
    temp_string: list[str] = []

    for question in vocab_json:
        temp_string.append(','.join([question["word"], question["op1"], question["op2"], question["op3"],
                                     question["op4"], question["correct"]]))

    return '#'.join(temp_string)


@router.post('/vocab', dependencies=[Depends(verify_token)])
def create_vocab_lobby(post: PostVocab):
    """
    Create a vocabulary lobby.

    Args:
        post (PostVocab): The vocabulary lobby data.

    Returns:
        dict: A dictionary containing the lobby ID.
    """

    if post.leader != 'singlePlayer':
        if post.leader == post.username:
            # the requester is the leader of the party

            member_list: list = get_members_in_party(post.leader)

            lobby_id = add_lobby(', '.join([post.leader] + member_list), 'vocabulary test')
            print(f"the new id is: {lobby_id}")

            for member in member_list:
                data: dict[str, any] = get_update_json(
                    {
                        "msg": f"your party leader started a vocabulary test game",
                        "lobby_id": lobby_id,
                        "game": "Vocab",
                        "type": "enter_game"
                    }
                )

                client_req[member] = data

            remove_party(post.leader)

        else:
            print("request from member")
    else:

        add_lobby(post.username, 'vocabulary test')

        remove_online(post.username)

        lobby_id = fetch_lobby_id_by_users(post.username)

        print(lobby_id)

        return {
            'lobby_id': lobby_id
        }

    remove_online(post.username)

    lobby_id = fetch_lobby_id_by_users(', '.join([post.leader] + get_members_in_party(post.leader)))

    print(lobby_id)

    return {
        'lobby_id': lobby_id
    }


@router.get('/getVocab', dependencies=[Depends(verify_token)])
def get_vocab_state(diff: str = Query(...)):
    """
    Get vocabulary state.

    Args:
        diff (str): The difficulty level.

    Returns:
        dict: A dictionary containing the vocabulary length and vocabulary data.
    """

    dict_ques = get_vocab_data(diff)

    return {
        'length': len(dict_ques),
        'vocab': dict_ques
    }


@router.post('/vocab/answer', dependencies=[Depends(verify_token)])
def post_vocab_answer(post: PostAnswer):
    """
    Post vocabulary answer.

    Args:
        post (PostAnswer): The answer data.

    Returns:
        dict: A dictionary containing whether the answer is correct.
    """
    dict_ques: dict[str, str] = get_vocab_data(post.diff, post.index)

    return {
        'correct': dict_ques['correct'] == post.answer
    }
