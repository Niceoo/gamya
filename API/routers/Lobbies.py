from fastapi import APIRouter, Query

from API.global_funcs import fetch_lobby_by_id, fetch_all_lobbies, fetch_lobby_by_id_raw

router = APIRouter()


@router.get('/getLobbyUsers')
def get_lobby_users(id: int = Query(...)):
    """
    Retrieve the users in a specific lobby.

    Args:
        id (int): The ID of the lobby.

    Returns:
        str: A string containing the users in the lobby.
    """
    lobby = fetch_lobby_by_id(id)
    if (users := lobby.get('users')) is not None:
        return users

    return 'no lobby found'


@router.get('/getOperationalLobbies')
def get_operational_lobbies():
    """
    Retrieve all operational lobbies.

    Returns:
        list: A list of operational lobbies.
    """
    lobbies = fetch_all_lobbies()

    operational_lobbies = []

    for lobby in lobbies:
        if lobby[4] == 'game ended':
            continue
        operational_lobbies.append(lobby)

    # print(operational_lobbies)

    return operational_lobbies


@router.get('/getLobbyData')
def get_lobby_data(lobby_id: int = Query(...)):
    """
    Retrieve data of a specific lobby.

    Args:
        lobby_id (int): The ID of the lobby.

    Returns:
        dict: Data of the lobby.
    """
    return fetch_lobby_by_id_raw(lobby_id)
