from fastapi import APIRouter, Query, Depends, HTTPException
from pydantic import BaseModel
from random import randint
from API.global_funcs import (verify_token, add_lobby, remove_online, fetch_lobby_id_by_users, get_members_in_party,
                              get_update_json, client_req, fetch_lobby_by_id, update_lobby_state, fetch_lobby_state,
                              remove_party)

router = APIRouter()


class PostConnect4(BaseModel):
    username: str
    leader: str


class PlaceModel(BaseModel):
    lobby_id: int
    index: int
    user_type: str


def get_type(value: int, opposite: bool = False) -> str:
    """
    Get the user type ('R' or 'B') based on the value and if it's opposite.

    Params:
    - value: int - The value to determine the user type.
    - opposite: bool - Flag to indicate if the user type should be opposite.

    Returns:
    - str: The user type ('R' or 'B').
    """
    if opposite:
        value = value ^ 1

    if value == 1:
        return 'R'
    else:
        return 'B'


def generate_state_string(state: list) -> str:
    """
    Generate a string representation of the Connect4 game state.

    Params:
    - state: list - The Connect4 game state.

    Returns:
    - str: The string representation of the game state.
    """

    state_str = ""

    for pos in state:
        if pos not in ['R', 'B']:
            state_str += ','
        else:
            state_str += f'{pos},'

    return state_str


def has_won(state: list[str]) -> str:
    """
    Check if there's a winner in the Connect4 game.

    Params:
    - state: list - The Connect4 game state.

    Returns:
    - str: The winner ('R', 'B', 'N') or empty string if the game isn't over yet.
    """
    board = [state[i:i + 7] for i in range(0, len(state), 7)]
    # Check horizontal
    for row in range(6):
        for col in range(4):
            if board[row][col] != "" and board[row][col] == board[row][col + 1] == board[row][col + 2] == board[row][col + 3]:
                return board[row][col]

    # Check vertical
    for row in range(3):
        for col in range(7):
            if board[row][col] != "" and board[row][col] == board[row + 1][col] == board[row + 2][col] == board[row + 3][col]:
                return board[row][col]

    # Check diagonals (positive slope)
    for row in range(3):
        for col in range(4):
            if board[row][col] != "" and board[row][col] == board[row + 1][col + 1] == board[row + 2][col + 2] == board[row + 3][col + 3]:
                return board[row][col]

    # Check diagonals (negative slope)
    for row in range(3):
        for col in range(3, 7):
            if board[row][col] != "" and board[row][col] == board[row + 1][col - 1] == board[row + 2][col - 2] == board[row + 3][col - 3]:
                return board[row][col]

    for row in range(6):
        for col in range(7):
            if board[row][col] == "":
                return ""

    return 'N'  # the board has filled, winner is None


def place_connect4(state: list, index: int, user_type: str) -> list:
    """
    Place a disk in the Connect4 game state.

    Params:
    - state: list - The Connect4 game state.
    - index: int - The index where the disk should be placed.
    - user_type: str - The type of user placing the disk.

    Returns:
    - list: The updated Connect4 game state.
    """

    for i in range(5, -1, -1):
        if state[index + i*7] in ['R', 'B']:
            continue  # already placed
        else:
            state[index + i*7] = user_type
            break

    return state


@router.post('/connect4', dependencies=[Depends(verify_token)])
def create_tictactoe_lobby(post: PostConnect4):
    """
    Create a Connect4 lobby.

    Params:
    - post: PostConnect4 - The POST request body containing username and leader.

    Returns:
    - dict: Dictionary containing lobby_id and user_type.
    """

    if post.leader != 'singlePlayer':
        if post.leader == post.username:
            # the requester is the leader of the party
            requester = 'leader'

            member_list: list = get_members_in_party(post.leader)

            if len(member_list) > 1:
                raise HTTPException(status_code=400, detail="Must be a party of 2")

            fresh_state: list[str] = ['' for _ in range(42)]
            rand: int = randint(0, 1)

            string_state: str = ",".join(fresh_state)

            string_state = string_state + f',#{rand}'

            print(f'creating lobby for {post.leader}, {post.username}')

            lobby_id: int = add_lobby(', '.join([post.leader] + member_list), 'Connect 4', state=string_state)
            print(f"the new id is: {lobby_id}")

            for member in member_list:
                data: dict[str, any] = get_update_json(
                    {
                        "msg": f"your party leader started a Connect 4 game",
                        "lobby_id": lobby_id,
                        "game": "Connect 4",
                        "type": "enter_game"
                    }
                )

                client_req[member] = data

            remove_party(post.leader)

        else:
            print("request from member")
            requester = 'member'
    else:
        # user is in single player mode.
        raise HTTPException(status_code=400, detail="Must be a party of 2")

    remove_online(post.username)

    lobby_id: int = fetch_lobby_id_by_users(', '.join([post.leader] + get_members_in_party(post.leader)))

    state: str = fetch_lobby_state(lobby_id)
    if requester == 'leader':
        user_type: str = get_type(int(state.split('#')[1]))
    else:
        user_type: str = get_type(int(state.split('#')[1]), True)

    return {
        'lobby_id': lobby_id,
        'user_type': user_type
    }


@router.get('/connect4/board', dependencies=[Depends(verify_token)])
def get_board(lobby_id: int = Query(...)):
    """
    Get the current Connect4 game board.

    Params:
    - lobby_id: int - The ID of the Connect4 lobby.

    Returns:
    - dict: A dictionary containing the Connect4 board and if the game is won.
    """
    lobby: dict[str, any] = fetch_lobby_by_id(lobby_id)

    state: str = lobby['state']

    return_json = {}

    if state[-1] in ['R', 'B', 'N']:
        return_json['won'] = state[-1]

    # print(f"state: {state.split('#')[0][:-1].split(',')}")

    return_json["board"] = state.split('#')[0][:-1].split(',')

    return return_json


@router.post('/connect4/place', dependencies=[Depends(verify_token)])
def place_at_index(post: PlaceModel):
    """
    Place a disk at a specific index in the Connect4 game.

    Params:
    - post: PlaceModel - The POST request body containing lobby_id, index, and user_type.

    Returns:
    - None
    """

    user_type: str = post.user_type
    index: int = post.index % 7

    lobby: dict[str, any] = fetch_lobby_by_id(post.lobby_id)

    state: list[str] = lobby['state'].split(',')

    updated_state: list = place_connect4(state[:-1], index, user_type)

    state_string: str = generate_state_string(updated_state) + state[-1]

    if won := has_won(updated_state):
        state_string += won

    update_lobby_state(post.lobby_id, state_string)
