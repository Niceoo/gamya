import sqlite3

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from uvicorn import Config, Server

from routers import html, Invites, Users, Vocab, Scores, Get2Zero, Tictactoe, Connect4, MathTest, GuessWrong, Lobbies

# Create FastAPI app
app = FastAPI()

# Include routers for different API endpoints
app.include_router(html.router)
app.include_router(Invites.router)
app.include_router(Users.router)
app.include_router(Vocab.router)
app.include_router(Scores.router)
app.include_router(Get2Zero.router)
app.include_router(Tictactoe.router)
app.include_router(Connect4.router)
app.include_router(MathTest.router)
app.include_router(GuessWrong.router)
app.include_router(Lobbies.router)

# Allowing CORS for all origins, methods, and headers
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def clear_tables(table_names: list[str]) -> None:
    """
    Clears the specified tables in the SQLite database.

    :param table_names: List of table names to clear
    """
    conn = sqlite3.connect('data.db')
    cursor = conn.cursor()
    for name in table_names:
        sql_query = f'DELETE FROM {name}'
        cursor.execute(sql_query)
    conn.commit()
    cursor.close()
    conn.close()
    print(f'Cleared tables: {table_names}')


def main() -> None:
    """
    Main function to start the FastAPI server.
    """
    # Uncomment the following lines to clear the tables before starting the server:
    # clear_tables(['users'])
    # clear_tables(['parties', 'lobbies'])

    config = Config(app=app, port=5000, log_level="warning", host="0.0.0.0")  # Change log_level to "info" for debug
    server = Server(config)

    print("Server up")
    server.run()

# Start the FastAPI application directly from the script
if __name__ == "__main__":
    main()
